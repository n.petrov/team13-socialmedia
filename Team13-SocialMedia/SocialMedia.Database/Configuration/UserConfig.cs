﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SocialMedia.Models;

namespace SocialMedia.Database.Configuration
{
    public class UserConfig : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(user => user.Id);

            builder.HasOne(usr => usr.Nationality)
                .WithMany(c => c.Users)
                .HasForeignKey(usr => usr.CountryId)
                .IsRequired(false);
        }
    }
}
