﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SocialMedia.Models;

namespace SocialMedia.Database.Configuration
{
    public class FavoritesListConfig : IEntityTypeConfiguration<FavoritesList>
    {
        public void Configure(EntityTypeBuilder<FavoritesList> builder)
        {
            builder.HasKey(e => new { e.UserId, e.PostId });

            builder.HasOne(d => d.User)
                .WithMany(b => b.Favorites)
                .HasForeignKey(b => b.UserId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(d => d.Post)
                .WithMany(p => p.Favorites)
                .HasForeignKey(d => d.PostId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
