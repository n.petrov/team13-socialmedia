﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SocialMedia.Models;

namespace SocialMedia.Database.Configuration
{
    public class PostDislikeConfig : IEntityTypeConfiguration<PostDislike>
    {
        public void Configure(EntityTypeBuilder<PostDislike> builder)
        {
            builder.HasKey(x => new { x.UserId, x.PostId});

            builder.HasOne(dislike => dislike.Post)
                .WithMany(post => post.Dislikes)
                .HasForeignKey(dislike => dislike.PostId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(dislike => dislike.User)
                .WithMany(user => user.PostDislikes)
                .HasForeignKey(dislike => dislike.UserId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
