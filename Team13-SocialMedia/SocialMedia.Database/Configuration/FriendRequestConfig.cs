﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SocialMedia.Models;

namespace SocialMedia.Database.Configuration
{
    public class FriendRequestConfig : IEntityTypeConfiguration<FriendRequest>
    {
        public void Configure(EntityTypeBuilder<FriendRequest> builder)
        {
            builder.HasOne(d => d.Sender)
                .WithMany(b => b.FriendRequests)
                .HasForeignKey(b => b.SenderId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
