﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SocialMedia.Models;

namespace SocialMedia.Database.Configuration
{
    public class CommentDislikeConfig : IEntityTypeConfiguration<CommentDislike>
    {
        public void Configure(EntityTypeBuilder<CommentDislike> builder)
        {
            builder.HasKey(x => new { x.UserId, x.CommentId });

            builder.HasOne(dislike => dislike.Comment)
                .WithMany(comment => comment.Dislikes)
                .HasForeignKey(dislike => dislike.CommentId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(dislike => dislike.User)
                .WithMany(user => user.CommentDislikes)
                .HasForeignKey(dislike => dislike.UserId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
