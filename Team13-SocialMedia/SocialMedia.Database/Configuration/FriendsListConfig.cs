﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SocialMedia.Models;

namespace SocialMedia.Database.Configuration
{
    public class FriendsListConfig : IEntityTypeConfiguration<FriendsList>
    {
        public void Configure(EntityTypeBuilder<FriendsList> builder)
        {
            builder.HasKey(x => new { x.UserId, x.FriendId });

            builder.HasOne(d => d.User)
                .WithMany(b => b.Friends)
                .HasForeignKey(b => b.UserId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
