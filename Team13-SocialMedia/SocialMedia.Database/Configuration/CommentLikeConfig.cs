﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SocialMedia.Models;

namespace SocialMedia.Database.Configuration
{
    public class CommentLikeConfig : IEntityTypeConfiguration<CommentLike>
    {
        public void Configure(EntityTypeBuilder<CommentLike> builder)
        {
            builder.HasKey(x => new { x.UserId, x.CommentId });

            builder.HasOne(like => like.Comment)
                .WithMany(comment => comment.Likes)
                .HasForeignKey(like => like.CommentId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(like => like.User)
                .WithMany(user => user.CommentLikes)
                .HasForeignKey(like => like.UserId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
