﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SocialMedia.Database.Configuration;
using SocialMedia.Models;
using System;

namespace SocialMedia.Database
{
    public class DBContext : IdentityDbContext<User, Role, int>
    {
        public DBContext(DbContextOptions<DBContext> options)
            : base(options)
        {
        }

        public DbSet<FriendRequest> FriendRequests { get; set; }
        public DbSet<FriendsList> FriendsList { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<PostLike> PostLikes { get; set; }
        public DbSet<PostDislike> PostDislikes { get; set; }
        public DbSet<CommentLike> CommentLikes{ get; set; }
        public DbSet<CommentDislike> CommentDislikes { get; set; }
        public DbSet<FavoritesList> FavoritesList { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new CommentConfig());
            builder.ApplyConfiguration(new FriendsListConfig());
            builder.ApplyConfiguration(new UserConfig());
            builder.ApplyConfiguration(new FavoritesListConfig());
            builder.ApplyConfiguration(new PostConfig());
            builder.ApplyConfiguration(new FriendRequestConfig());
            builder.ApplyConfiguration(new PostLikeConfig());
            builder.ApplyConfiguration(new PostDislikeConfig());
            builder.ApplyConfiguration(new CommentLikeConfig());
            builder.ApplyConfiguration(new CommentDislikeConfig());
            base.OnModelCreating(builder);

            #region CountrySeed
            builder.Entity<Country>().HasData(
                new Country
                {
                    Id = 1,
                    Name = "Afghanistan"
                },
                new Country
                {
                    Id = 2,
                    Name = "Albania"
                },
                new Country
                {
                    Id = 3,
                    Name = "Algeria"
                },
                new Country
                {
                    Id = 4,
                    Name = "Andorra"
                },
                new Country
                {
                    Id = 5,
                    Name = "Angola"
                },
                new Country
                {
                    Id = 6,
                    Name = "Antigua and Barbuda"
                },
                new Country
                {
                    Id = 7,
                    Name = "Argentina"
                },
                new Country
                {
                    Id = 8,
                    Name = "Armenia"
                },
                new Country
                {
                    Id = 9,
                    Name = "Australia"
                },
                new Country
                {
                    Id = 10,
                    Name = "Austria"
                },
                new Country
                {
                    Id = 11,
                    Name = "Azerbaijan"
                },
                new Country
                {
                    Id = 12,
                    Name = "The Bahamas"
                },
                new Country
                {
                    Id = 13,
                    Name = "Bahrain"
                },
                new Country
                {
                    Id = 14,
                    Name = "Bangladesh"
                },
                new Country
                {
                    Id = 15,
                    Name = "Barbados"
                },
                new Country
                {
                    Id = 16,
                    Name = "Belarus"
                },
                new Country
                {
                    Id = 17,
                    Name = "Belgium"
                },
                new Country
                {
                    Id = 18,
                    Name = "Belize"
                },
                new Country
                {
                    Id = 19,
                    Name = "Benin"
                },
                new Country
                {
                    Id = 20,
                    Name = "Bhutan"
                },
                new Country
                {
                    Id = 21,
                    Name = "Bolivia"
                },
                new Country
                {
                    Id = 22,
                    Name = "Bosnia and Herzegovina"
                },
                new Country
                {
                    Id = 23,
                    Name = "Botswana"
                },
                new Country
                {
                    Id = 24,
                    Name = "Brazil"
                },
                new Country
                {
                    Id = 25,
                    Name = "Brunei"
                },
                new Country
                {
                    Id = 26,
                    Name = "Bulgaria"
                },
                new Country
                {
                    Id = 27,
                    Name = "Burkina Faso"
                },
                new Country
                {
                    Id = 28,
                    Name = "Burundi"
                },
                new Country
                {
                    Id = 29,
                    Name = "Cambodia"
                },
                new Country
                {
                    Id = 30,
                    Name = "Cameroon"
                },
                new Country
                {
                    Id = 31,
                    Name = "Canada"
                },
                new Country
                {
                    Id = 32,
                    Name = "Cape Verde"
                },
                new Country
                {
                    Id = 33,
                    Name = "Central African Republic"
                },
                new Country
                {
                    Id = 34,
                    Name = "Chad"
                },
                new Country
                {
                    Id = 35,
                    Name = "Chile"
                },
                new Country
                {
                    Id = 36,
                    Name = "China"
                },
                new Country
                {
                    Id = 37,
                    Name = "Colombia"
                },
                new Country
                {
                    Id = 38,
                    Name = "Comoros"
                },
                new Country
                {
                    Id = 39,
                    Name = "Republic of the Congo"
                },
                new Country
                {
                    Id = 40,
                    Name = "Democratic Republic of the Congo"
                },
                new Country
                {
                    Id = 41,
                    Name = "Costa Rica"
                },
                new Country
                {
                    Id = 42,
                    Name = "Cote d'Ivoire"
                },
                new Country
                {
                    Id = 43,
                    Name = "Croatia"
                },
                new Country
                {
                    Id = 44,
                    Name = "Cuba"
                },
                new Country
                {
                    Id = 45,
                    Name = "Cyprus"
                },
                new Country
                {
                    Id = 46,
                    Name = "Czech Republic"
                },
                new Country
                {
                    Id = 47,
                    Name = "Denmark"
                },
                new Country
                {
                    Id = 48,
                    Name = "Djibouti"
                },
                new Country
                {
                    Id = 49,
                    Name = "Dominica"
                },
                new Country
                {
                    Id = 50,
                    Name = "Dominican Republic"
                },
                new Country
                {
                    Id = 51,
                    Name = "East Timor"
                },
                new Country
                {
                    Id = 52,
                    Name = "Ecuador"
                },
                new Country
                {
                    Id = 53,
                    Name = "Egypt"
                },
                new Country
                {
                    Id = 54,
                    Name = "El Salvador"
                },
                new Country
                {
                    Id = 55,
                    Name = "Equatorial Guinea"
                },
                new Country
                {
                    Id = 56,
                    Name = "Eritrea"
                },
                new Country
                {
                    Id = 57,
                    Name = "Estonia"
                },
                new Country
                {
                    Id = 58,
                    Name = "Ethiopia"
                },
                new Country
                {
                    Id = 59,
                    Name = "Fiji"
                },
                new Country
                {
                    Id = 60,
                    Name = "Finland"
                },
                new Country
                {
                    Id = 61,
                    Name = "France"
                },
                new Country
                {
                    Id = 62,
                    Name = "Gabon"
                },
                new Country
                {
                    Id = 63,
                    Name = "The Gambia"
                },
                new Country
                {
                    Id = 64,
                    Name = "Georgia"
                },
                new Country
                {
                    Id = 65,
                    Name = "Germany"
                },
                new Country
                {
                    Id = 66,
                    Name = "Ghana"
                },
                new Country
                {
                    Id = 67,
                    Name = "Greece"
                },
                new Country
                {
                    Id = 68,
                    Name = "Grenada"
                },
                new Country
                {
                    Id = 69,
                    Name = "Guatemala"
                },
                new Country
                {
                    Id = 70,
                    Name = "Guinea"
                },
                new Country
                {
                    Id = 71,
                    Name = "Guinea-Bissau"
                },
                new Country
                {
                    Id = 72,
                    Name = "Guyana"
                },
                new Country
                {
                    Id = 73,
                    Name = "Haiti"
                },
                new Country
                {
                    Id = 74,
                    Name = "Honduras"
                },
                new Country
                {
                    Id = 75,
                    Name = "Hungary"
                },
                new Country
                {
                    Id = 76,
                    Name = "Iceland"
                },
                new Country
                {
                    Id = 77,
                    Name = "India"
                },
                new Country
                {
                    Id = 78,
                    Name = "Indonesia"
                },
                new Country
                {
                    Id = 79,
                    Name = "Iran"
                },
                new Country
                {
                    Id = 80,
                    Name = "Iraq"
                },
                new Country
                {
                    Id = 81,
                    Name = "Ireland"
                },
                new Country
                {
                    Id = 82,
                    Name = "Israel"
                },
                new Country
                {
                    Id = 83,
                    Name = "Italy"
                },
                new Country
                {
                    Id = 84,
                    Name = "Jamaica"
                },
                new Country
                {
                    Id = 85,
                    Name = "Japan"
                },
                new Country
                {
                    Id = 86,
                    Name = "Jordan"
                },
                new Country
                {
                    Id = 87,
                    Name = "Kazakhstan"
                },
                new Country
                {
                    Id = 88,
                    Name = "Kenya"
                },
                new Country
                {
                    Id = 89,
                    Name = "Kiribati"
                },
                new Country
                {
                    Id = 90,
                    Name = "North Korea"
                },
                new Country
                {
                    Id = 91,
                    Name = "South Korea"
                },
                new Country
                {
                    Id = 92,
                    Name = "Kosovo"
                },
                new Country
                {
                    Id = 93,
                    Name = "Kuwait"
                },
                new Country
                {
                    Id = 94,
                    Name = "Kyrgyzstan"
                },
                new Country
                {
                    Id = 95,
                    Name = "Laos"
                },
                new Country
                {
                    Id = 96,
                    Name = "Latvia"
                },
                new Country
                {
                    Id = 97,
                    Name = "Lebanon"
                },
                new Country
                {
                    Id = 98,
                    Name = "Lesotho"
                },
                new Country
                {
                    Id = 99,
                    Name = "Liberia"
                },
                new Country
                {
                    Id = 100,
                    Name = "Libya"
                },
                new Country
                {
                    Id = 101,
                    Name = "Liechtenstein"
                },
                new Country
                {
                    Id = 102,
                    Name = "Lithuania"
                },
                new Country
                {
                    Id = 103,
                    Name = "Luxembourg"
                },
                new Country
                {
                    Id = 104,
                    Name = "Macedonia"
                },
                new Country
                {
                    Id = 105,
                    Name = "Madagascar"
                },
                new Country
                {
                    Id = 106,
                    Name = "Malawi"
                },
                new Country
                {
                    Id = 107,
                    Name = "Malaysia"
                },
                new Country
                {
                    Id = 108,
                    Name = "Maldives"
                },
                new Country
                {
                    Id = 109,
                    Name = "Mali"
                },
                new Country
                {
                    Id = 110,
                    Name = "Malta"
                },
                new Country
                {
                    Id = 111,
                    Name = "Marshall Islands"
                },
                new Country
                {
                    Id = 112,
                    Name = "Mauritania"
                },
                new Country
                {
                    Id = 113,
                    Name = "Mauritius"
                },
                new Country
                {
                    Id = 114,
                    Name = "Mexico"
                },
                new Country
                {
                    Id = 115,
                    Name = "Micronesia"
                },
                new Country
                {
                    Id = 116,
                    Name = "Moldova"
                },
                new Country
                {
                    Id = 117,
                    Name = "Monaco"
                },
                new Country
                {
                    Id = 118,
                    Name = "Mongolia"
                },
                new Country
                {
                    Id = 119,
                    Name = "Montenegro"
                },
                new Country
                {
                    Id = 120,
                    Name = "Morocco"
                },
                new Country
                {
                    Id = 121,
                    Name = "Mozambique"
                },
                new Country
                {
                    Id = 122,
                    Name = "Myanmar"
                },
                new Country
                {
                    Id = 123,
                    Name = "Namibia"
                },
                new Country
                {
                    Id = 124,
                    Name = "Nauru"
                },
                new Country
                {
                    Id = 125,
                    Name = "Nepal"
                },
                new Country
                {
                    Id = 126,
                    Name = "Netherlands"
                },
                new Country
                {
                    Id = 127,
                    Name = "New Zealand"
                },
                new Country
                {
                    Id = 128,
                    Name = "Nicaragua"
                },
                new Country
                {
                    Id = 129,
                    Name = "Niger"
                },
                new Country
                {
                    Id = 130,
                    Name = "Nigeria"
                },
                new Country
                {
                    Id = 131,
                    Name = "Norway"
                },
                new Country
                {
                    Id = 132,
                    Name = "Oman"
                },
                new Country
                {
                    Id = 133,
                    Name = "Pakistan"
                },
                new Country
                {
                    Id = 134,
                    Name = "Palau"
                },
                new Country
                {
                    Id = 135,
                    Name = "Panama"
                },
                new Country
                {
                    Id = 136,
                    Name = "Papua New Guinea"
                },
                new Country
                {
                    Id = 137,
                    Name = "Paraguay"
                },
                new Country
                {
                    Id = 138,
                    Name = "Peru"
                },
                new Country
                {
                    Id = 139,
                    Name = "Philippines"
                },
                new Country
                {
                    Id = 140,
                    Name = "Poland"
                },
                new Country
                {
                    Id = 141,
                    Name = "Portugal"
                },
                new Country
                {
                    Id = 142,
                    Name = "Qatar"
                },
                new Country
                {
                    Id = 143,
                    Name = "Romania"
                },
                new Country
                {
                    Id = 144,
                    Name = "Russia"
                },
                new Country
                {
                    Id = 145,
                    Name = "Rwanda"
                },
                new Country
                {
                    Id = 146,
                    Name = "Saint Kitts and Nevis"
                },
                new Country
                {
                    Id = 147,
                    Name = "Saint Lucia"
                },
                new Country
                {
                    Id = 148,
                    Name = "Saint Vincent and the Grenadines"
                },
                new Country
                {
                    Id = 149,
                    Name = "Samoa"
                },
                new Country
                {
                    Id = 150,
                    Name = "San Marino"
                },
                new Country
                {
                    Id = 151,
                    Name = "Sao Tome and Principe"
                },
                new Country
                {
                    Id = 152,
                    Name = "Saudi Arabia"
                },
                new Country
                {
                    Id = 153,
                    Name = "Senegal"
                },
                new Country
                {
                    Id = 154,
                    Name = "Serbia"
                },
                new Country
                {
                    Id = 155,
                    Name = "Seychelles"
                },
                new Country
                {
                    Id = 156,
                    Name = "Sierra Leone"
                },
                new Country
                {
                    Id = 157,
                    Name = "Singapore"
                },
                new Country
                {
                    Id = 158,
                    Name = "Slovakia"
                },
                new Country
                {
                    Id = 159,
                    Name = "Slovenia"
                },
                new Country
                {
                    Id = 160,
                    Name = "Solomon Islands"
                },
                new Country
                {
                    Id = 161,
                    Name = "Somalia"
                },
                new Country
                {
                    Id = 162,
                    Name = "South Africa"
                },
                new Country
                {
                    Id = 163,
                    Name = "South Sudan"
                },
                new Country
                {
                    Id = 164,
                    Name = "Spain"
                },
                new Country
                {
                    Id = 165,
                    Name = "Sri Lanka"
                },
                new Country
                {
                    Id = 166,
                    Name = "Sudan"
                },
                new Country
                {
                    Id = 167,
                    Name = "Suriname"
                },
                new Country
                {
                    Id = 168,
                    Name = "Swaziland"
                },
                new Country
                {
                    Id = 169,
                    Name = "Sweden"
                },
                new Country
                {
                    Id = 170,
                    Name = "Switzerland"
                },
                new Country
                {
                    Id = 171,
                    Name = "Syria"
                },
                new Country
                {
                    Id = 172,
                    Name = "Taiwan"
                },
                new Country
                {
                    Id = 173,
                    Name = "Tajikistan"
                },
                new Country
                {
                    Id = 174,
                    Name = "Tanzania"
                },
                new Country
                {
                    Id = 175,
                    Name = "Thailand"
                },
                new Country
                {
                    Id = 176,
                    Name = "Togo"
                },
                new Country
                {
                    Id = 177,
                    Name = "Tonga"
                },
                new Country
                {
                    Id = 178,
                    Name = "Trinidad and Tobago"
                },
                new Country
                {
                    Id = 179,
                    Name = "Tunisia"
                },
                new Country
                {
                    Id = 180,
                    Name = "Turkey"
                },
                new Country
                {
                    Id = 181,
                    Name = "Turkmenistan"
                },
                new Country
                {
                    Id = 182,
                    Name = "Tuvalu"
                },
                new Country
                {
                    Id = 183,
                    Name = "Uganda"
                },
                new Country
                {
                    Id = 184,
                    Name = "Ukraine"
                },
                new Country
                {
                    Id = 185,
                    Name = "United Arab Emirates"
                },
                new Country
                {
                    Id = 186,
                    Name = "United Kingdom"
                },
                new Country
                {
                    Id = 187,
                    Name = "United States of America"
                },
                new Country
                {
                    Id = 188,
                    Name = "Uruguay"
                },
                new Country
                {
                    Id = 189,
                    Name = "Uzbekistan"
                },
                new Country
                {
                    Id = 190,
                    Name = "Vanuatu"
                },
                new Country
                {
                    Id = 191,
                    Name = "Vatican City"
                },
                new Country
                {
                    Id = 192,
                    Name = "Venezuela"
                },
                new Country
                {
                    Id = 193,
                    Name = "Vietnam"
                },
                new Country
                {
                    Id = 194,
                    Name = "Yemen"
                },
                new Country
                {
                    Id = 195,
                    Name = "Zambia"
                },
                new Country
                {
                    Id = 196,
                    Name = "Zimbabwe"
                });
            #endregion

            #region RoleSeed
            builder.Entity<Role>().HasData(
                new Role
                {
                    Id = 1,
                    Name = "User",
                    NormalizedName = "USER"
                },

                new Role
                {
                    Id = 2,
                    Name = "Admin",
                    NormalizedName = "ADMIN"
                });
            #endregion

            #region UserSeed
            var hasher = new PasswordHasher<User>();

            var adminUser = new User();
            var normalUser = new User();
            var thirdUser = new User();

            adminUser.Id = 1;
            adminUser.UserName = "admin@admin.com";
            adminUser.FirstName = "Pavel";
            adminUser.LastName = "Katsarov";
            adminUser.Age = 32;
            adminUser.About = "I am an admin!";
            adminUser.JobDescription = "I work as an admin full time.";
            adminUser.PhoneNumber = "+35988888888";
            adminUser.NormalizedUserName = "ADMIN@ADMIN.COM";
            adminUser.Email = "admin@admin.com";
            adminUser.NormalizedEmail = "ADMIN@ADMIN.COM";
            adminUser.PasswordHash = hasher.HashPassword(adminUser, "Admin@123");
            adminUser.SecurityStamp = Guid.NewGuid().ToString();
            adminUser.CountryId = 26;
            adminUser.ProfilePicture = new byte[1];
            adminUser.PictureType = "image/png";

            normalUser.Id = 2;
            normalUser.UserName = "user@user.com";
            normalUser.FirstName = "Pesho";
            normalUser.LastName = "Popov";
            normalUser.Age = 26;
            normalUser.About = "I am a regular user!";
            normalUser.JobDescription = "I do not have a job.";
            normalUser.PhoneNumber = "+3595555555";
            normalUser.NormalizedUserName = "USER@USER.COM";
            normalUser.Email = "user@user.com";
            normalUser.NormalizedEmail = "USER@USER.COM";
            normalUser.PasswordHash = hasher.HashPassword(normalUser, "User@123");
            normalUser.SecurityStamp = Guid.NewGuid().ToString();
            normalUser.CountryId = 26;
            normalUser.ProfilePicture = new byte[1];
            normalUser.PictureType = "image/png";

            thirdUser.Id = 3;
            thirdUser.UserName = "third@third.com";
            thirdUser.FirstName = "Veselin";
            thirdUser.LastName = "Topalov";
            thirdUser.Age = 45;
            thirdUser.About = "Bulgarian chess grandmaster and former FIDE World Chess Champion.";
            thirdUser.JobDescription = "Chess player.";
            thirdUser.PhoneNumber = "+35933333333";
            thirdUser.NormalizedUserName = "THIRD@THIRD.COM";
            thirdUser.Email = "third@third.com";
            thirdUser.NormalizedEmail = "THIRD@THIRD.COM";
            thirdUser.PasswordHash = hasher.HashPassword(normalUser, "User@123");
            thirdUser.SecurityStamp = Guid.NewGuid().ToString();
            thirdUser.CountryId = 26;
            thirdUser.ProfilePicture = new byte[1];
            thirdUser.PictureType = "image/png";

            builder.Entity<User>().HasData
            (
                adminUser,

                normalUser,

                thirdUser
            );

            builder.Entity<IdentityUserRole<int>>().HasData(
                new IdentityUserRole<int>
                {
                    RoleId = 1,
                    UserId = 2
                },
                new IdentityUserRole<int>
                {
                    RoleId = 1,
                    UserId = 3
                },
                new IdentityUserRole<int>
                {
                    RoleId = 2,
                    UserId = 1
                });
            #endregion

            #region FriendRequestSeed
            builder.Entity<FriendRequest>().HasData(
                new FriendRequest
                {
                    Id = 1,
                    ReceiverId = 2,
                    SenderId = 1,
                    RequestBody = "I would like to be your friend!",
                    CreatedOn = DateTime.Now
                });
            #endregion

            #region PostSeed
            builder.Entity<Post>().HasData(
                new Post
                {
                    Id = 1,
                    UserId = 2,
                    Picture = new byte[1],
                    PictureType = "image/png",
                    Title = "DeepMind Solved Protein Folding",
                    Body = "Proteins are essential to life, supporting practically all its functions. They are large complex molecules, made up of chains of amino acids, and what a protein does largely depends on its unique 3D structure. Figuring out what shapes proteins fold into is known as the “protein folding problem”, and has stood as a grand challenge in biology for the past 50 years. In a major scientific advance, the latest version of our AI system AlphaFold has been recognised as a solution to this grand challenge by the organisers of the biennial Critical Assessment of protein Structure Prediction (CASP). This breakthrough demonstrates the impact AI can have on scientific discovery and its potential to dramatically accelerate progress in some of the most fundamental fields that explain and shape our world.",
                    CreatedOn = DateTime.Now
                },
                new Post
                {
                    Id = 2,
                    UserId = 2,
                    Picture = new byte[1],
                    PictureType = "image/png",
                    Title = "Python Pip 20.3 Released with new resolver",
                    Body = "New resolver: Check version equality with packaging.version to avoid edge cases if a wheel used different version normalization logic in its filename and metadata. (#9083)",
                    CreatedOn = DateTime.Now
                },
                new Post
                {
                    Id = 3,
                    UserId = 2,
                    Picture = new byte[1],
                    PictureType = "image/png",
                    Title = "How many registers does an x86-64 CPU have?",
                    Body = "The general-purpose registers (or GPRs) are the primary registers in the x86-64 register model. As their name implies, they are the only registers that are general purpose: each has a set of conventional uses1, but programmers are generally free to ignore those conventions and use them as they please2.",
                    CreatedOn = DateTime.Now
                },
                new Post
                {
                    Id = 4,
                    UserId = 1,
                    Picture = new byte[1],
                    PictureType = "image/png",
                    Title = "WeChat permanently closes account after user sets CCP-offensive password",
                    Body = "Recently, I decided to change the password. Never one to miss an opportunity to test boundaries, I used the most CCP-offensive password I could think of:",
                    CreatedOn = DateTime.Now
                },
                new Post
                {
                    Id = 5,
                    UserId = 3,
                    Picture = new byte[1],
                    PictureType = "image/png",
                    Title = "Reddit app got 50M downloads by making mobile web experience miserable",
                    Body = "Anyone who's tried to browse Reddit on their phone's web browser will be familiar with the constant popups prompting users to download the mobile app instead. Well, it looks like making their mobile site virtually unusable has paid off for the site, as its official Android app has just passed 50 million downloads on the Play Store.",
                    CreatedOn = DateTime.Now
                },
                new Post
                {
                    Id = 6,
                    UserId = 1,
                    Picture = new byte[1],
                    PictureType = "image/png",
                    Title = "Linus Torvalds – “Somebody is pushing complete garbage for unclear reasons.”",
                    Body = "I'm sure there is some lawyer there who says \"we'll have to go through motions to protect against a lawsuit\". But legal reasons do not make for good technology, or good patches that I should apply.",
                    CreatedOn = DateTime.Now
                },
                new Post
                {
                    Id = 7,
                    UserId = 3,
                    Picture = new byte[1],
                    PictureType = "image/png",
                    Title = "Google bans Zoom from employees' computers",
                    Body = "Google has banned the popular videoconferencing software Zoom from its employees’ devices, BuzzFeed News has learned. Zoom, a competitor to Google’s own Meet app, has seen an explosion of people using it to work and socialize from home and has become a cultural touchstone during the coronavirus pandemic.",
                    CreatedOn = DateTime.Now
                },
                new Post
                {
                    Id = 8,
                    UserId = 3,
                    Picture = new byte[1],
                    PictureType = "image/png",
                    Title = "Half of American workers would rather work from home forever: poll",
                    Body = "For many companies and workers that have never worked remotely this is all completely new. Companies hastily threw together guidelines and crossed their fingers in hopes that nothing would break. Workers wondered if it would all work. ",
                    CreatedOn = DateTime.Now
                },
                new Post
                {
                    Id = 9,
                    UserId = 1,
                    Picture = new byte[1],
                    PictureType = "image/png",
                    Title = "California bans private prisons and immigration detention centers",
                    Body = "California moved to end the use of private, for-profit lockups in America’s largest state prison system as well as in federal immigration detention centers in the state under a measure signed into law on Friday by Governor Gavin Newsom.",
                    CreatedOn = DateTime.Now
                },
                new Post
                {
                    Id = 10,
                    UserId = 3,
                    Picture = new byte[1],
                    PictureType = "image/png",
                    Title = "Zoom’s encryption has links to China, researchers discover",
                    Body = "Meetings on Zoom, the increasingly popular video conferencing service, are encrypted using an algorithm with serious, well-known weaknesses, and sometimes using keys issued by servers in China, even when meeting participants are all in North America, according to researchers at the University of Toronto.",
                    CreatedOn = DateTime.Now
                },
                new Post
                {
                    Id = 11,
                    UserId = 1,
                    Picture = new byte[1],
                    PictureType = "image/png",
                    Title = "DuckDuckGo Is Now a Default Search Engine Option on Android in the EU",
                    Body = "European regulators are forcing Google to present Android users with the option to choose their own default search engine. The option to choose default search providers has always been available on Android devices, but it’s something users would have to seek out on their own in the settings menu.",
                    CreatedOn = DateTime.Now
                });
            #endregion

            #region PostPictureSeed
            #endregion
        }
    }
}
