﻿using System;
using System.Collections.Generic;

namespace SocialMedia.Web.Models
{
    public class PostViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public DateTime CreatedOn { get; set; }
        public byte[] Picture { get; set; }
        public string PictureType { get; set; }
        public int? UserId { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public int Likes { get; set; }
        public int Dislikes { get; set; }
        public double EngagementFactor { get; set; }
        public ICollection<CommentViewModel> Comments { get; set; }
    }
}
