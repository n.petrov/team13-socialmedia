﻿using System;

namespace SocialMedia.Web.Models
{
    public class CommentViewModel
    {
        public int Id { get; set; }
        public string Body { get; set; }
        public string CommenterName { get; set; }
        public int CommenterId { get; set; }
        public byte[] CommenterPfp { get; set; }
        public int Likes { get; set; }
        public int Dislikes { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime EditedOn { get; set; }
    }
}
