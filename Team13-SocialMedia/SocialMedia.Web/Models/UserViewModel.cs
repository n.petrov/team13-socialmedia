﻿using System;

namespace SocialMedia.Web.Models
{
    public class UserViewModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }
        public string Nationality { get; set; }
        public string Role { get; set; }
        public string PhoneNumber { get; set; }
        public string About { get; set; }
        public DateTime DateOfBirth { get; set; }
        public byte[] ProfilePicture { get; set; }
        public string Address { get; set; }
        public string BanDescription { get; set; }
        public DateTime BannedOn { get; set; }
        public bool IsBanned { get; set; }
        /// <summary>
        /// <para>Sets user profile visibility:</para>
        /// <br>0 = Private</br>
        /// <br>1 = Friends Only</br>
        /// <br>2 = Public</br>
        /// </summary>
        public byte PrivacySetting { get; set; }
        public string JobDescription { get; set; }
    }
}
