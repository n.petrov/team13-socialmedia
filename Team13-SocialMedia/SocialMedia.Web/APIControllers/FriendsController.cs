﻿using System.Security.Claims;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SocialMedia.Services.Contracts;
using SocialMedia.Services.DTOs;

namespace SocialMedia.Web.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class FriendsController : ControllerBase
    {
        private readonly IFriendsService _friendsService;

        public FriendsController(IFriendsService friendsService)
        {
            _friendsService = friendsService;
        }

        // GET: api/FriendsLists
        [HttpGet]
        public async Task<IActionResult> GetAllFriends()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var friendList = await _friendsService.GetFriends(int.Parse(userId));
            
            return Ok(friendList);
        }

        //// GET: api/FriendsLists/5
        //[HttpGet("{userid}/{friendid?}")]
        //public async Task<IActionResult> Get(int userId, int? friendId)
        //{
        //    if (friendId == null)
        //    {
        //        return Ok(await _friendsListService.GetAllFriend(userId));
        //    }

        //    return Ok(await _friendsListService.GetFriend(userId, (int)friendId));
        //}

        // POST: api/FriendsLists
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        //[HttpPost]
        //public async Task<IActionResult> PostFriendsList([FromQuery (Name = "userid")] int userId, 
        //                                                 [FromQuery(Name ="friendid")]int friendId)
        //{
        //    FriendsListDTO friendList = await _friendsListService.AddFriend(userId, friendId);

        //    return Ok(friendList);
        //}

        // DELETE: api/FriendsLists/5
        //[HttpDelete]
        //public async Task<IActionResult> DeleteFriendsList([FromQuery(Name = "userid")] int userId,
        //                                                   [FromQuery(Name = "friendid")] int friendId)
        //{
        //    return Ok(await _friendsListService.DeleteFriend(userId, friendId));
        //}

        //private bool FriendsListExists(int? id)
        //{
        //    return _context.FriendsLists.Any(e => e.UserId == id);
        //}
    }
}
