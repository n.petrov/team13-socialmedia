﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SocialMedia.Services.Contracts;

namespace SocialMedia.Web.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        private readonly ICommentService _commentService;

        public CommentsController(ICommentService commentService)
        {
            _commentService = commentService;
        }

        // GET: api/Comments
        //[HttpGet]
        //public async Task<ActionResult<IEnumerable<Comment>>> GetComments()
        //{
        //    //return  ;
        //}

        //// GET: api/Comments/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<Comment>> GetComment(int? id)
        //{
        //    var comment = await _context.Comments.FindAsync(id);

        //    if (comment == null)
        //    {
        //        return NotFound();
        //    }

        //    return comment;
        //}

        // PUT: api/Comments/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{commentid}")]
        public async Task<IActionResult> PutComment(int commentId, [FromBody] string body)
        {
            if (body == null)
            {
                return BadRequest();
            }

            return Ok(await _commentService.Edit(commentId, body));
        }

        // POST: api/Comments/1
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost("{commentid}")]
        public Task<IActionResult> CommentComment(int commentId, int userId, [FromBody] string body)
        {
            // if (body == null)
            // {
            //     return BadRequest();
            // }
            // 
            // return Ok(await _commentService.ReplyToComment(commentId, userId, body));

            throw new System.NotImplementedException();
        }  
        
        // POST: api/Comments/Post/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost("post/{commentid}")]
        public async Task<IActionResult> PostComment(int postId, int userId, [FromBody] string body)
        {
            if (body == null)
            {
                return BadRequest();
            }

            return Ok(await _commentService.ReplyToPost(postId, userId, body));
        }

        // DELETE: api/Comments/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteComment(int id)
        {
    
            return Ok(await _commentService.Remove(id));
        }
    }
}
