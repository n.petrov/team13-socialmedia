﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SocialMedia.Services.Contracts;
using SocialMedia.Services.DTOs;

namespace SocialMedia.Web.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class PostsController : ControllerBase
    {
        private readonly IPostService _postService;

        public PostsController(IPostService postService)
        {
            _postService = postService;
        }

        // GET: api/Posts
        [HttpGet]
        public async Task<IActionResult> GetPosts()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var postsList = await _postService.GetUserPosts(int.Parse(userId));
            
            return Ok(postsList);
        }

        // GET: api/Posts/5/1
        //[HttpGet("{userId}/{postId?}")]
        //public async Task<IActionResult> GetPost(int userId, int? postId)
        //{
        //    if (postId == null)
        //    {
        //        var postsList = await _postService.GetAllPosts(userId);
        //        return Ok(postsList);
        //    }

        //    var post = _postService.GetPost(userId, (int)postId);

        //    return Ok(post);
        //}

        // GET: api/Posts/newsfeed/5
        [HttpGet("newsfeed")]
        public async Task<IActionResult> GetNewsFeed()
        {
                var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                var newsFeedList = await _postService.GenerateFeed(int.Parse(userId));

                return Ok(newsFeedList);           
        }

        //// PUT: api/Posts/5
        //// To protect from overposting attacks, enable the specific properties you want to bind to, for
        //// more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutPost(int id, [FromBody] PostDTO post)
        //{
        //    if (post == null)
        //    {
        //        return BadRequest();
        //    }

        //    await _postService.EditPost(id, post);

        //    return NoContent();
        //}

        // POST: api/Posts
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<IActionResult> PostPost([FromBody]PostDTO post)
        {
            if (post == null)
            {
                return BadRequest();
            }

            await _postService.CreatePost(post);
            return Ok(post);
        }

        //// DELETE: api/Posts/5
        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeletePost(int id)
        //{
        //    return Ok(await _postService.DeletePost(id));
        //}
    }
}
