﻿using System.Security.Claims;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SocialMedia.Services.Contracts;
using SocialMedia.Web.Mapper;

namespace SocialMedia.Web.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IRoleService _roleService;

        public UsersController(IUserService userService, IRoleService roleService)
        {
            _roleService = roleService;
            _userService = userService;
        }

        // GET: api/Users
        [Authorize]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            //var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var userProfile = await _userService.Get(id);
            
            return Ok(userProfile.ToViewModel(await _roleService.GetRole(id)));
        }

        // GET: api/Users
        [HttpGet("")]
        public async Task<IActionResult> GetUser([FromQuery(Name = "sortBy")] string sortBy, [FromQuery(Name = "order")] string order)
        {
            var users = (await _userService.GetMany(sortBy, order))
                .Select(async x => x.ToViewModel(await _roleService.GetRole(x.Id)));

            if (users == null)
            {
                return NotFound();
            }

            return Ok(users);
        }
    }
}
