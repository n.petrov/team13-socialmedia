﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace SocialMedia.Web.Middlewares
{
    public class PageNotFoundMiddleware
    {
        private readonly RequestDelegate next;

        public PageNotFoundMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            await this.next.Invoke(httpContext);

            if (httpContext.Response.StatusCode == 404)
            {
                httpContext.Response.Redirect("/Home/PageNotFound");
            }
        }
    }
}