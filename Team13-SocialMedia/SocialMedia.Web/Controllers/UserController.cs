﻿using Microsoft.AspNetCore.Mvc;
using SocialMedia.Services.Contracts;
using SocialMedia.Services.DTOs;
using SocialMedia.Web.Mapper;
using SocialMedia.Web.Models;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SocialMedia.Web.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly IFriendsService _friendsService;
        private readonly IPostService _postService;

        public UserController(IUserService userService, IFriendsService friendsService, IPostService postService)
        {
            _userService = userService;
            _friendsService = friendsService;
            _postService = postService;
        }

        public async Task<IActionResult> Index(int id)
        {
            ViewData["Posts"] = (await _postService.GetUserPosts(id))
                .Select(x => x.ToViewModel());

            return await VerifyAccessibility(id);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(PostDTO model, int id)
        {
            if (ModelState.IsValid)
            {
                model.UserId = id;

                await _postService.CreatePost(model);
            }

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Search(string sortBy = "Id", string order = "asc", string searchQuery = "")
        {
            ViewData["Users"] = (await _userService.GetMany(sortBy, order, searchQuery))
                .Select(x => x.ToViewModel(""));

            return View();
        }

        public async Task<IActionResult> Friends(int id)
        {
            ViewData["Friends"] = (await _friendsService.GetFriends(id))
                .Select(x => x.ToViewModel(""));

            return await VerifyAccessibility(id);
        }

        public async Task<IActionResult> About(int id)
        {
            return await VerifyAccessibility(id);
        }

        public ActionResult Restricted()
        {
            return View();
        }

        /// <summary>
        /// Restricts access to page based on profile privacy setting.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Restricted View if profile is restricted for current user.</returns>
        private async Task<IActionResult> VerifyAccessibility(int id)
        {
            bool isCurrentUserLoggedIn = int.TryParse(
                User.FindFirstValue(ClaimTypes.NameIdentifier),
                out int currentUserId);

            if (id == currentUserId)
                return RedirectToAction("Index", "Profile");

            var userDTO = await _userService.Get(id);

            if (userDTO == null)
                return RedirectToAction("PageNotFound", "Home");

            UserViewModel user = userDTO.ToViewModel("");

            if (user.PrivacySetting == 2)
            {
                return View(user);
            }
            else if (user.PrivacySetting == 1)
            {
                if (isCurrentUserLoggedIn)
                {
                    if (await _friendsService.IsFriendsWith(currentUserId, id))
                    {
                        return View(user);
                    }
                }
            }

            return RedirectToAction(nameof(Restricted));
        }
    }
}
