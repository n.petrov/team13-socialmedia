﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SocialMedia.Services.Contracts;
using SocialMedia.Services.DTOs;
using SocialMedia.Web.Mapper;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.Web.Controllers
{
    public class AdminController : Controller
    {
        private readonly IUserService _userService;
        private readonly IRoleService _roleService;

        public AdminController(IUserService userService, IRoleService roleService)
        {
            _userService = userService;
            _roleService = roleService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Users()
        {
            return View((await _userService.GetMany())
                .Select(x => x.ToViewModel(_roleService.GetRole(x.Id).Result)));
        }

        public async Task<ActionResult> UserDetails(int id)
        {
            var user = await _userService.Get(id);
            return View(user.ToViewModel(await _roleService.GetRole(id)));
        }

        public async Task<ActionResult> EditUser(int id)
        {
            var user = await _userService.Get(id);
            user.Role = await _roleService.GetRole(id);
            ViewData["Roles"] = new SelectList(await _roleService.GetAllRoles());
            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditUser(int id, UserDTO userDTO)
        {
            await _userService.ChangeRole(id, userDTO.Role);
            return RedirectToAction(nameof(Users));
        }

        public async Task<ActionResult> DeleteUser(int id)
        {
            var user = await _userService.Get(id);
            return View(user.ToViewModel(await _roleService.GetRole(id)));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteUser(int id, IFormCollection collection)
        {
            try
            {
                await _userService.RemoveFromDb(id);
                return RedirectToAction(nameof(Users));
            }
            catch
            {
                return View();
            }
        }

        public async Task<ActionResult> BanUser(int id)
        {
            var user = await _userService.Get(id);
            return View(user.ToViewModel(await _roleService.GetRole(id)));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> BanUser(int id, UserDTO user)
        {
            try
            {
                string desc = Request.Form["banDescription"];
                await _userService.Ban(id, desc);
                return RedirectToAction(nameof(Users));
            }
            catch
            {
                return View();
            }
        }

        public async Task<ActionResult> UnbanUser(int id)
        {
            var user = await _userService.Get(id);
            return View(user.ToViewModel(await _roleService.GetRole(id)));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UnbanUser(int id, UserDTO user)
        {
            try
            {
                await _userService.Unban(id);
                return RedirectToAction(nameof(Users));
            }
            catch
            {
                return View();
            }
        }
    }
}
