﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SocialMedia.Services.Contracts;
using SocialMedia.Services.DTOs;
using System;
using System.IO;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SocialMedia.Web.Controllers
{
    [Authorize]
    public class SettingsController : Controller
    {
        private readonly IUserService _userService;
        private readonly ICountryService _countryService;
        private readonly IPictureService _pictureService;

        public SettingsController(IUserService userService, ICountryService countryService, IPictureService pictureService)
        {
            _userService = userService;
            _countryService = countryService;
            _pictureService = pictureService;
        }

        private int UserId
        {
            get
            {
                return int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));
            }
        }

        private async Task<ViewResult> GetView()
        {
            return View(await _userService.Get(this.UserId));
        }

        public async Task<IActionResult> Index()
        {
            ViewData["CountryList"] = new SelectList(await _countryService.GetMany(), "Id", "Name");

            return await GetView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(UserDTO model)
        {
            bool result = true;

            result &= int.TryParse(Request.Form["birthDay"], out int day);
            string month = Request.Form["birthMonth"];
            result &= int.TryParse(Request.Form["birthYear"], out int year);
            result &= DateTime.TryParse($"{year}/{month}/{day}", out DateTime dateOfBirth);

            if (result)
            {
                int userId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));

                model.DateOfBirth = dateOfBirth;
                model.About = Request.Form["aboutSection"].ToString();
                model.Age = (DateTime.Now - dateOfBirth).Days / 365;

                await _userService.Update(userId, model);
            }

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Password()
        {
            return await GetView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Password(int id)
        {
            string oldPassword = Request.Form["oldPassword"].ToString();
            string newPassword = Request.Form["newPassword"].ToString();

            int userId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));

            await _userService.ChangePassword(userId, oldPassword, newPassword);

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Account()
        {
            ViewData["PrivacyOptions"] = new (string Name, int Value)[]
            {
                ("Private", 0),
                ("Friends Only", 1),
                ("Public", 2)
            };

            return await GetView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Account(int id)
        {
            if (byte.TryParse(Request.Form["PrivacySetting"], out byte setting))
            {
                await _userService.UpdatePrivacy(this.UserId, setting);
            }

            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UploadPfp(IFormFile file)
        {
            await _pictureService.EditProfilePicture(UserId, file);

            return RedirectToAction(nameof(Index));
        }
    }
}
