﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SocialMedia.Services.Contracts;
using SocialMedia.Services.DTOs;
using SocialMedia.Web.Mapper;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SocialMedia.Web.Controllers
{
    public class PostController : Controller
    {
        private readonly IPostService _postService;
        private readonly IUserService _userService;
        private readonly IRatingService _ratingService;
        private readonly ICommentService _commentService;
        private readonly IPictureService _pictureService;

        public PostController(IPostService postService, IUserService userService,
            ICommentService commentService, IRatingService ratingService,
            IPictureService pictureService)
        {
            _postService = postService;
            _commentService = commentService;
            _ratingService = ratingService;
            _userService = userService;
            _pictureService = pictureService;
        }

        private int CurentUserId => int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));

        private async Task<UserDTO> GetCurrentUser()
        {
            return await _userService.Get(CurentUserId);
        }

        public async Task<IActionResult> Index(string sortBy = "likes", string order = "desc",
            string searchQuery = "", int pageSize = 2, int page = 1)
        {
            ViewData["Posts"] = (await _postService.GetAll(sortBy, order,
            searchQuery, pageSize, page))
            .Select(x => x.ToViewModel());

            return View();
        }

        public async Task<IActionResult> GetImage(int postId)
        {
            PostDTO post = await _postService.GetPost(postId);

            if (post == null)
                return RedirectToAction("PageNotFound", "Home");

            return File(post.Picture, post.PictureType);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SubmitPost(IFormFile file)
        {
            UserDTO tmpUser = await GetCurrentUser();

            (byte[] Data, string DataType) data = await _pictureService.GetFileData(file);

            PostDTO post = new PostDTO
            {
                UserFirstName = tmpUser.FirstName,
                UserLastName = tmpUser.LastName,
                UserId = tmpUser.Id,
                Body = Request.Form["postBody"],
                Title = Request.Form["postTitle"],
                Picture = data.Data,
                PictureType = data.DataType
            };

            await _postService.CreatePost(post);

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> PostComment()
        {
            int userId = int.Parse(Request.Form["userId"]);
            int postId = int.Parse(Request.Form["postId"]);
            string text = Request.Form["commentText"];

            await _commentService.ReplyToPost(postId, userId, text);

            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> LikePost()
        {
            int postId = int.Parse(Request.Form["postId"]);
            await _ratingService.LikePost(this.CurentUserId, postId);

            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> DislikePost()
        {
            int postId = int.Parse(Request.Form["postId"]);
            await _ratingService.DislikePost(this.CurentUserId, postId);

            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> LikeComment()
        {
            int commentId = int.Parse(Request.Form["commentId"]);
            await _ratingService.LikeComment(this.CurentUserId, commentId);

            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> DislikeComment()
        {
            int commentId = int.Parse(Request.Form["commentId"]);
            await _ratingService.DislikeComment(this.CurentUserId, commentId);

            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> DeleteComment()
        {
            int commentId = int.Parse(Request.Form["commentId"]);
            await _commentService.Remove(commentId);

            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> DeletePost()
        {
            int postId = int.Parse(Request.Form["postId"]);
            await _postService.DeletePost(postId);

            return RedirectToAction(nameof(Index));
        }
    }
}
