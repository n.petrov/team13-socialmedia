﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SocialMedia.Services.Contracts;
using SocialMedia.Web.Mapper;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SocialMedia.Web.Controllers
{
    [Authorize]
    public class ProfileController : Controller
    {
        private readonly IUserService _userService;
        private readonly IFriendsService _friendsService;
        private readonly IFriendRequestService _requestService;
        private readonly IPostService _postService;

        public ProfileController(IUserService userService, IFriendsService friendsService,
            IPostService postService, IFriendRequestService requestService)
        {
            _userService = userService;
            _friendsService = friendsService;
            _postService = postService;
            _requestService = requestService;
        }

        private int UserId
        {
            get
            {
                return int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));
            }
        }

        private async Task<ViewResult> GetView()
        {
            return View((await _userService.Get(this.UserId)).ToViewModel(""));
        }

        public async Task<IActionResult> Index()
        {
            ViewData["Posts"] = (await _postService.GetUserPosts(UserId))
                .Select(x => x.ToViewModel());

            return await GetView();
        }

        public async Task<IActionResult> Friends()
        {
            ViewData["Friends"] = (await _friendsService.GetFriends(UserId))
                .Select(x => x.ToViewModel(""));

            ViewData["Incoming"] = (await _requestService.GetIncomingRequests(UserId))
                .Select(x => x.ToViewModel());

            ViewData["Outgoing"] = (await _requestService.GetOutgoingRequests(UserId))
                .Select(x => x.ToViewModel());

            return await GetView();
        }

        [HttpPost]
        public async Task<IActionResult> SendRequest()
        {
            int receiverId = int.Parse(Request.Form["requestId"]);

            await _requestService.SendRequest(this.UserId, receiverId);

            return RedirectToAction(nameof(Friends));
        }

        [HttpPost]
        public async Task<IActionResult> AcceptRequest()
        {
            int requestId = int.Parse(Request.Form["requestId"]);

            await _requestService.AcceptRequest(requestId);

            return RedirectToAction(nameof(Friends));
        }

        [HttpPost]
        public async Task<IActionResult> RejectRequest()
        {
            int requestId = int.Parse(Request.Form["requestId"]);

            await _requestService.RemoveRequest(requestId);

            return RedirectToAction(nameof(Friends));
        }

        [HttpPost]
        public async Task<IActionResult> CancelRequest()
        {
            int requestId = int.Parse(Request.Form["requestId"]);

            await _requestService.RemoveRequest(requestId);

            return RedirectToAction(nameof(Friends));
        }

        [HttpPost]
        public async Task<IActionResult> Unfriend()
        {
            int friendId = int.Parse(Request.Form["requestId"]);

            await _friendsService.DeleteFriend(this.UserId, friendId);

            return RedirectToAction(nameof(Friends));
        }

        public async Task<IActionResult> About()
        {
            return await GetView();
        }
    }
}
