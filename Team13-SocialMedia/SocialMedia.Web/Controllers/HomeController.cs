﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SocialMedia.Services.Contracts;
using SocialMedia.Services.DTOs;
using SocialMedia.Web.Mapper;
using SocialMedia.Web.Models;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SocialMedia.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly IPostService _postService;
        private readonly IUserService _userService;
        private readonly IRatingService _ratingService;
        private readonly ICommentService _commentService;

        public HomeController(IPostService postService, IUserService userService,
            ICommentService commentService, IRatingService ratingService)
        {
            _postService = postService;
            _commentService = commentService;
            _ratingService = ratingService;
            _userService = userService;
        }

        private int CurentUserId => int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));

        private async Task<UserDTO> GetCurrentUser()
        {
            return await _userService.Get(CurentUserId);
        }

        public async Task<IActionResult> Index(int pageSize = 20, int page = 1)
        {
            UserDTO current = await GetCurrentUser();

            if (current == null)
                return RedirectToAction(nameof(PageNotFound));

            ViewData["Posts"] = (await _postService.GenerateFeed(current.Id, pageSize, page))
                .Select(x => x.ToViewModel())
                .OrderByDescending(x => x.EngagementFactor);

            return View(current.ToViewModel(""));
        }

        public async Task<IActionResult> GetImage(int userId)
        {
            if (userId == -1)
                return File(new byte[1], "image/png");

            UserDTO user = await _userService.Get(userId);

            return File(user.ProfilePicture, user.PictureType);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel
            {
                RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier
            });
        }

        public IActionResult PageNotFound()
        {
            return View();
        }
    }
}
