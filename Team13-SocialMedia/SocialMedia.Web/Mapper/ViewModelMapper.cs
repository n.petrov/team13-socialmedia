﻿using SocialMedia.Services.DTOs;
using SocialMedia.Web.Models;
using System.Linq;

namespace SocialMedia.Web.Mapper
{
    public static class ViewModelMapper
    {
        public static UserViewModel ToViewModel(this UserDTO model)
        {
            return new UserViewModel
            {
                Id = model.Id,
                UserName = model.UserName,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                Age = model.Age,
                Nationality = model.Nationality,
                Role = "",
                DateOfBirth = model.DateOfBirth,
                ProfilePicture = model.ProfilePicture,
                Address = model.Address,
                BanDescription = model.BanDescription,
                BannedOn = model.BannedOn,
                IsBanned = model.IsBanned,
                JobDescription = model.JobDescription,
                PrivacySetting = model.PrivacySetting,
                PhoneNumber = model.PhoneNumber,
                About = model.About
            };
        }

        public static UserViewModel ToViewModel(this UserDTO model, string role)
        {
            return new UserViewModel
            {
                Id = model.Id,
                UserName = model.UserName,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                Age = model.Age,
                Nationality = model.Nationality,
                Role = role,
                DateOfBirth = model.DateOfBirth,
                ProfilePicture = model.ProfilePicture,
                Address = model.Address,
                BanDescription = model.BanDescription,
                BannedOn = model.BannedOn,
                IsBanned = model.IsBanned,
                JobDescription = model.JobDescription,
                PrivacySetting = model.PrivacySetting,
                PhoneNumber = model.PhoneNumber,
                About = model.About
            };
        }

        public static PostViewModel ToViewModel(this PostDTO model)
        {
            return new PostViewModel
            {
                Id = model.Id,
                Title = model.Title,
                Body = model.Body,
                UserId = model.UserId,
                UserFirstName = model.UserFirstName,
                UserLastName = model.UserLastName,
                CreatedOn = model.CreatedOn,
                Picture = model.Picture,
                PictureType = model.PictureType,
                Comments = model.Comments.Select(x => x.ToViewModel()).ToList(),
                Likes = model.Likes,
                Dislikes = model.Dislikes,
                EngagementFactor = model.Comments.Count * 1.7 + (model.Likes + model.Dislikes) * 1.2
            };
        }

        public static FriendRequestViewModel ToViewModel(this FriendRequestDTO model)
        {
            return new FriendRequestViewModel
            {
                Id = model.Id,
                CreatedOn = model.CreatedOn,
                ReceiverId = model.ReceiverId,
                SenderId = model.SenderId,
                SenderFullName = model.SenderFullName,
                ReceiverFullName = model.ReceiverFullName,
                SenderPfp = model.SenderPfp,
                ReceiverPfp = model.ReceiverPfp,
                RequestBody = model.RequestBody
            };
        }

        public static CommentViewModel ToViewModel(this CommentDTO model)
        {
            return new CommentViewModel
            {
                Id = model.Id,
                CommenterId = (int)model.UserId,
                Body = model.Body,
                CommenterName = model.CommenterName,
                CommenterPfp = model.CommenterPfp,
                CreatedOn = model.CreatedOn,
                Likes = model.Likes,
                Dislikes = model.Dislikes,
                EditedOn = model.EditedOn
            };
        }
    }
}
