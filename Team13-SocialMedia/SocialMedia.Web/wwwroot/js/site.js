﻿function loadUserContainer(url, el) {
    $('#fillContainer').load(url + ' #fillContainer', function () {
        if (el.className == "active") {
            el.className = "";
        } else {
            el.className = "active";
        }
    });
}

function handle(e, url) {
    if (e.keyCode === 13) {
        e.preventDefault();

        window.location = url;
    }
}

$('#loadMorePostBtn').click(function () {
    var nextPage = parseInt($('#pageNumber').val()) + 1;
    $.ajax({
        type: 'GET',
        url: '/Post/Index',
        data: { page: nextPage },
        success: function (res) {
            var newPosts = $(res).find('#postHolder');

            $('#postHolder').append(newPosts);
            $('#pageNumber').val(nextPage);
        }
    });
});

function ajaxFriendRequest(requestId, request) {
    formData = new FormData;
    formData.append("requestId", requestId);

    $.ajax({
        type: 'POST',
        url: "/Profile/" + request,
        data: formData,
        contentType: false,
        processData: false,

        statusCode: {
            401: function () {
                window.location = "/Home/Index/";
            }
        }
    });

    return false;
}

function ajaxLikePost(postId) {
    formData = new FormData;
    formData.append("postId", postId);

    $.ajax({
        type: 'POST',
        url: "/Post/LikePost",
        data: formData,
        contentType: false,
        processData: false,

        statusCode: {
            401: function () {
                window.location = "/Home/Index/";
            }
        },

        success: function (res) {

        }
    });

    return false;
}

function ajaxDislikePost(postId) {
    formData = new FormData;
    formData.append("postId", postId);

    $.ajax({
        type: 'POST',
        url: "/Post/DislikePost",
        data: formData,
        contentType: false,
        processData: false,

        statusCode: {
            401: function () {
                window.location = "/Home/Index/";
            }
        },

        success: function (res) {

        }
    });

    return false;
}

function ajaxDeletePost(postId) {
    formData = new FormData;
    formData.append("postId", postId);

    $.ajax({
        type: 'POST',
        url: "/Post/DeletePost",
        data: formData,
        contentType: false,
        processData: false,

        statusCode: {
            401: function () {
                window.location = "/Home/Index/";
            }
        },

        success: function (res) {

        }
    });

    return false;
}

function ajaxLikeComment(commentId) {
    formData = new FormData;
    formData.append("commentId", commentId);

    $.ajax({
        type: 'POST',
        url: "/Post/LikeComment",
        data: formData,
        contentType: false,
        processData: false,

        statusCode: {
            401: function () {
                window.location = "/Home/Index/";
            }
        },

        success: function (res) {

        }
    });

    return false;
}

function ajaxDislikeComment(commentId) {
    formData = new FormData;
    formData.append("commentId", commentId);

    $.ajax({
        type: 'POST',
        url: "/Post/DislikeComment",
        data: formData,
        contentType: false,
        processData: false,

        statusCode: {
            401: function () {
                window.location = "/Home/Index/";
            }
        },

        success: function (res) {

        }
    });

    return false;
}

function ajaxDeleteComment(commentId) {
    formData = new FormData;
    formData.append("commentId", commentId);

    $.ajax({
        type: 'POST',
        url: "/Post/DeleteComment",
        data: formData,
        contentType: false,
        processData: false,

        statusCode: {
            401: function () {
                window.location = "/Home/Index/";
            }
        },

        success: function (res) {
            
        }
    });

    return false;
}

function jqueryPost(form) {
    if (event.keyCode == 13) {
        $.ajax({
            type: 'POST',
            url: "/Post/PostComment",
            data: new FormData(form),
            contentType: false,
            processData: false,

            statusCode: {
                401: function () {
                    window.location = "/Home/Index/";
                }
            },

            success: function () {
                $('#commentText').val('');
                var comment = "<li>" +
                    "<div class='comet-avatar'>" +
                    "<img src='@comment.CommenterPfp' alt=''></div><div class='we-comment'>" +
                    "<div class='coment-head'>" +
                    "<h5><a href='time-line.html' title=''>@comment.CommenterName</a></h5>" +
                    "<span >@comment.CreatedOn</span > <a class='we-reply' href='#' title='Reply'><i class='fa fa-reply'></i></a>" +
                    "</div > <p>@comment.Body</p></div ></li > ";

                divId = `#commentSection${parseInt(test.get('postId'))}`;
                $('#commentSection').prepend(comment);
            }
        });

        return false;
    }
}

function redirectToSearch(q) {
        alert("/Post/Index?searchQuery=" + q.value);
    try {
        window.location = "/Post/Index?searchQuery=" + q;
    }catch (ex){
        alert(ex);
    }
}

function redirectToAdmin() {
    window.location = "/Admin/";
}

function redirectToProfile() {
    window.location = "/Profile/";
}

function redirectToSettings() {
    window.location = "/Settings/";
}

function redirectToFriends() {
    window.location = "/Profile/Friends/";
}

function redirectToHome() {
    window.location = "/Home/Index/";
}

function redirectToAPI() {
    window.location = "/swagger/";
}