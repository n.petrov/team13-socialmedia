﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SocialMedia.Models
{
    public class User : IdentityUser<int>
    {
        [Required]
        public DateTime CreatedOn { get; set; }
        public DateTime EditedOn { get; set; }
        public DateTime DeletedOn { get; set; }
        [Required]
        [MinLength(2)]
        [MaxLength(30)]
        public string FirstName { get; set; }
        [Required]
        [MinLength(2)]
        [MaxLength(30)]
        public string LastName { get; set; }
        [Required]
        [Range(18, 121)]
        public int Age { get; set; }
        public string About { get; set; }
        public int CountryId { get; set; }
        public Country Nationality { get; set; }
        public DateTime DateOfBirth { get; set; }
        public byte[] ProfilePicture { get; set; } = new byte[1];
        public string PictureType { get; set; } = "image/png";
        public string Address { get; set; }
        public DateTime BannedOn { get; set; }
        public DateTime UnbannedOn { get; set; }
        public string BanDescription { get; set; }
        public string JobDescription { get; set; }
        /// <summary>
        /// <para>Sets user profile visibility:</para>
        /// <br>0 = Private</br>
        /// <br>1 = Friends Only</br>
        /// <br>2 = Public</br>
        /// </summary>
        public byte PrivacySetting { get; set; } = 2;
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<PostLike> PostLikes { get; set; } = new List<PostLike>();
        public virtual ICollection<PostDislike> PostDislikes { get; set; } = new List<PostDislike>();
        public virtual ICollection<CommentLike> CommentLikes { get; set; } = new List<CommentLike>();
        public virtual ICollection<CommentDislike> CommentDislikes { get; set; } = new List<CommentDislike>();
        public virtual ICollection<Post> Posts { get; set; }
        public virtual ICollection<FriendsList> Friends { get; set; }
        public virtual ICollection<FriendRequest> FriendRequests { get; set; }
        public virtual ICollection<FavoritesList> Favorites { get; set; }
    }
}
