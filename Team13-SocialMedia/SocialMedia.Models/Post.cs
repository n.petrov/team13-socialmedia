﻿using SocialMedia.Models.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SocialMedia.Models
{
    public class Post : Entity
    {
        [MinLength(2)]
        [MaxLength(100)]
        public string Title { get; set; }
        [MinLength(2)]
        [MaxLength(1000)]
        public string Body { get; set; }
        public int? UserId { get; set; }
        public User User { get; set; }
        //public byte PrivacySettings { get; set; }
        public byte[] Picture { get; set; }
        public string PictureType { get; set; }
        public virtual ICollection<Comment> Comments { get; set; } = new List<Comment>();
        public virtual ICollection<PostLike> Likes { get; set; } = new List<PostLike>();
        public virtual ICollection<PostDislike> Dislikes { get; set; } = new List<PostDislike>();
        public virtual ICollection<FavoritesList> Favorites { get; set; } = new List<FavoritesList>();
    }
}
