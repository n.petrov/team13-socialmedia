﻿using SocialMedia.Models.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SocialMedia.Models
{
    public class Country
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MinLength(4)]
        [MaxLength(44)]
        public string Name { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
