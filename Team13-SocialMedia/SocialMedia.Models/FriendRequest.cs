﻿using SocialMedia.Models.Common;

namespace SocialMedia.Models
{
    public class FriendRequest : Entity
    {
        public int? SenderId { get; set; }
        public User Sender { get; set; }
        public int? ReceiverId { get; set; }
        public User Receiver { get; set; }
        public string RequestBody { get; set; }
    }
}
