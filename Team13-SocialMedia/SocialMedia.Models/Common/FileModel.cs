﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SocialMedia.Models.Common
{
    public abstract class FileModel
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string FileType { get; set; }
        public string Extension { get; set; }
        public DateTime CreatedOn { get; set; }
        [Required]
        public byte[] Data { get; set; }
    }
}
