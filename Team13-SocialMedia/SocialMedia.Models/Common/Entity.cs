﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SocialMedia.Models.Common
{
    public abstract class Entity
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public DateTime CreatedOn { get; set; }
        public DateTime EditedOn { get; set; }
        public DateTime DeletedOn { get; set; }
    }
}
