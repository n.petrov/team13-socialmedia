﻿using SocialMedia.Models.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SocialMedia.Models
{
    public class Comment : Entity
    {
        public int? UserId { get; set; }
        public User User { get; set; }
        public int PostId { get; set; }
        public Post Post { get; set; }
        [Required]
        [MinLength(3)]
        [MaxLength(200)]
        public string Body { get; set; }
        public virtual ICollection<CommentLike> Likes { get; set; } = new List<CommentLike>();
        public virtual ICollection<CommentDislike> Dislikes { get; set; } = new List<CommentDislike>();
    }
}
