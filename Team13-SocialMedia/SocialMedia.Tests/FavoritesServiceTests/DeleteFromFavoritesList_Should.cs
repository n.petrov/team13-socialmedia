﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialMedia.Database;
using SocialMedia.Models;
using SocialMedia.Services;
using SocialMedia.Services.DTOs;
using SocialMedia.Services.Mapper;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.Tests.FavoritesServiceTests
{
    [TestClass]
    public class DeleteFriend_Should
    {
        [TestMethod]
        public async Task Return_True_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Return_True_When_ParamsAreValid));

            var country = new Country
            {
                Id = 1,
                Name = "Greece"
            };
            var user = new User
            {
                Id = 1,
                FirstName = "Dwein",
                LastName = "Jonson",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };
            var post = new Post
            {
                Id = 1,
                Body = "It is test post!",
                UserId = 1,
                User = user
            };
            var post1 = new Post
            {
                Id = 2,
                Body = "It is test post 1!",
                UserId = 1,
                User = user
            }; var post2 = new Post
            {
                Id = 3,
                Body = "It is test post 2!",
                UserId = 1,
                User = user
            };

            using (var arrangeContext = new DBContext(options))
            {
                var sut = new PostService(arrangeContext, new FriendsService(arrangeContext));

                await arrangeContext.Countries.AddAsync(country);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.Posts.AddAsync(post);
                await arrangeContext.Posts.AddAsync(post1);
                await sut.CreatePost(post2.ToDTO());
                await arrangeContext.SaveChangesAsync();
            }
            //Act   
            using (var actContext = new DBContext(options))
            {
                var sut = new FavoritesService(actContext);

                var actual = sut.AddToFavoritesList(1, 3);
            }

            using (var assertContext = new DBContext(options))
            {
                var sut = new FavoritesService(assertContext);

                //var actual = await assertContext.FavoritesList.FirstOrDefaultAsync(f => f.UserId == 1 && f.PostId == 3);
                var actual = await sut.DeleteFromFavoritesList(1, 3);
                //Assert
                Assert.IsTrue(actual);
            }
        }

        [TestMethod]
        public async Task ReturnFalse_When_PostIsNull()
        {

            //Arrange
            var options = Utils.GetOptions(nameof(ReturnFalse_When_PostIsNull));
                       
            using (var assertContext = new DBContext(options))
            {
                var sut = new FavoritesService(assertContext);
                var actual =await sut.DeleteFromFavoritesList(1, 3);
                //var a = await actContext.Posts.FirstOrDefaultAsync(p => p.Id == 3);

                Assert.IsFalse(actual);
            }
        }       
    }
}
