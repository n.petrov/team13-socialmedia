﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialMedia.Database;
using SocialMedia.Models;
using SocialMedia.Services;
using System;
using System.Threading.Tasks;
using SocialMedia.Services.Mapper;
using Microsoft.EntityFrameworkCore;

namespace SocialMedia.Tests.PostServiceTests
{
    [TestClass]
    public class CreatePost_Should
    {
        [TestMethod]
        public async Task Return_Post_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(CreatePost_Should) + nameof(Return_Post_When_ParamsAreValid));
            var country = new Country
            {
                Id = 1,
                Name = "Greece"
            };
            var user = new User
            {
                Id = 1,
                FirstName = "Dwein",
                LastName = "Jonson",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };
            var post = new Post
            {
                Id = 1,
                Body = "It is test post!",
                UserId = 1,       
                User = user
            };
           

            using (var arrangeContext = new DBContext(options))
            {
                var sut = new PostService(arrangeContext, new FriendsService(arrangeContext));
                await arrangeContext.Countries.AddAsync(country);
                await arrangeContext.Users.AddAsync(user);
                await sut.CreatePost(post.ToDTO());
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new DBContext(options))
            {
                var sut = new PostService(assertContext, new FriendsService(assertContext));
                //Act               
                //var actual = await sut.GetPost(1);
                var actual = await assertContext.Posts.FirstOrDefaultAsync(p => p.Id == 1);

                //Assert
                Assert.AreEqual("It is test post!", actual.Body);         
            }
        }

        [TestMethod]
        public async Task ReturnNull_When_PostNotFound()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(CreatePost_Should) + nameof(ReturnNull_When_PostNotFound));

            //Act & Assert
            using (var actContext = new DBContext(options))
            {
                var sut = new PostService(actContext, new FriendsService(actContext));
                var actual = await sut.GetPost(1);

                //Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await sut.Create(countryDTO));
                Assert.IsNull(actual);
            }
        }
    }
}
