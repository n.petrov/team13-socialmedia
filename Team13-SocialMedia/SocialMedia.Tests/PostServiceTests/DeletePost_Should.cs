﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialMedia.Database;
using SocialMedia.Models;
using SocialMedia.Services;
using SocialMedia.Services.Mapper;
using System;
using System.Threading.Tasks;

namespace SocialMedia.Tests.PostServiceTests
{
    [TestClass]
    public class DeletePost_Should
    {
        [TestMethod]
        public async Task ReturnTrue_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(DeletePost_Should) + nameof(ReturnTrue_When_ParamsAreValid));

            var country = new Country
            {
                Id = 1,
                Name = "Greece"
            };
            var user = new User
            {
                Id = 1,
                FirstName = "Dwein",
                LastName = "Jonson",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };
            var post = new Post
            {
                Id = 1,
                Body = "It is test post!",
                UserId = 1,
                User = user
            };


            using (var arrangeContext = new DBContext(options))
            {
                var sut = new PostService(arrangeContext, new FriendsService(arrangeContext));
                
                await arrangeContext.Countries.AddAsync(country);
                await arrangeContext.Users.AddAsync(user);
                await sut.CreatePost(post.ToDTO());
                
                await arrangeContext.SaveChangesAsync();
            }

            //Act
            //using (var actContext = new DBContext(options))
            //{
            //    var sut = new CommentService(actContext);
            //    await sut.Remove(2);
            //    await actContext.SaveChangesAsync();
            //}

            //Assert
            using (var assertContext = new DBContext(options))
            {
                var sut = new PostService(assertContext, new FriendsService(assertContext));
                var actual = await sut.DeletePost(1);

                Assert.IsTrue(actual);
            }
        }

        [TestMethod]
        public async Task ReturnFalse_When_Params_Are_Not_Valid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(DeletePost_Should) + nameof(ReturnFalse_When_Params_Are_Not_Valid));

            //var providerMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new DBContext(options))
            {
                var sut = new PostService(assertContext, new FriendsService(assertContext));

                //Act
                var result = await sut.DeletePost(1);

                //Assert
                Assert.IsFalse(result);
            }
        }

    }
}
