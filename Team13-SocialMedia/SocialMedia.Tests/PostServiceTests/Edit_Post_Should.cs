﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialMedia.Database;
using SocialMedia.Models;
using SocialMedia.Services;
using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SocialMedia.Services.Mapper;

namespace SocialMedia.Tests.PostServiceTests
{
    [TestClass]
    public class Edit_Post_Should
    {
        [TestMethod]
        public async Task Return_Updatet_Post_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Edit_Post_Should) + nameof(Return_Updatet_Post_When_ParamsAreValid));

            var country = new Country
            {
                Id = 1,
                Name = "Greece"
            };
            var user = new User
            {
                Id = 1,
                FirstName = "Dwein",
                LastName = "Jonson",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };
            var post = new Post
            {
                Id = 1,
                Body = "It is a test post!",
                UserId = 1,
                User = user
            }; 
            var editPost = new Post
            {
                Id = 1,
                Body = "It is a edited test post!",
                UserId = 1,
                User = user
            };

            //Arange
            using (var arrangeContext = new DBContext(options))
            {
                var sut = new PostService(arrangeContext, new FriendsService(arrangeContext));

                await arrangeContext.Countries.AddAsync(country);
                await arrangeContext.Users.AddAsync(user);
                await sut.CreatePost(post.ToDTO());

                await arrangeContext.SaveChangesAsync();
            }

            //Act
            using (var actContext = new DBContext(options))
            {
                var sut = new PostService(actContext, new FriendsService(actContext));

                await sut.EditPost(1, editPost.ToDTO());
                await actContext.SaveChangesAsync();
            }
            //Asert
            using (var assertContext = new DBContext(options))
            {
                var sut = new PostService(assertContext, new FriendsService(assertContext));              
                var actual = await assertContext.Posts.FirstOrDefaultAsync(c => c.Id == 1);

                Assert.AreEqual(actual.Body, "It is a edited test post!");
            }
        }

        [TestMethod]
        public async Task ReturnFalse_When_Post_Are_Not_Valid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Edit_Post_Should) + nameof(ReturnFalse_When_Post_Are_Not_Valid));
            var country = new Country
            {
                Id = 1,
                Name = "Greece"
            };
            var user = new User
            {
                Id = 1,
                FirstName = "Dwein",
                LastName = "Jonson",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };           
            var editPost = new Post
            {
                Id = 1,
                Body = "It is a edited test post!",
                UserId = 1,
                User = user
            };

            //Arange
            using (var arrangeContext = new DBContext(options))
            {
                var sut = new PostService(arrangeContext, new FriendsService(arrangeContext));

                await arrangeContext.Countries.AddAsync(country);
                await arrangeContext.Users.AddAsync(user);                

                await arrangeContext.SaveChangesAsync();
            }

            //Act & Asert
            using (var assertContext = new DBContext(options))
            {
                var sut = new PostService(assertContext, new FriendsService(assertContext));

                var actual = await sut.EditPost(1, editPost.ToDTO());

                Assert.IsNull(actual);
            }           
        }
    }
}
