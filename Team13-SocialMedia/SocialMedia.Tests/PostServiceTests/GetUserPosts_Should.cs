﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialMedia.Database;
using SocialMedia.Models;
using SocialMedia.Services;
using SocialMedia.Services.Mapper;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.Tests.PostServiceTests
{
    [TestClass]
    public class GetUserPosts_Should
    {
        [TestMethod]
        public async Task ReturnAllPosts_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnAllPosts_When_ParamsAreValid));
            var country = new Country
            {
                Id = 1,
                Name = "Greece"
            };
            var user = new User
            {
                Id = 1,
                FirstName = "Dwein",
                LastName = "Jonson",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };
            var post = new Post
            {
                Id = 1,
                Body = "It is test post!",
                UserId = 1,
                User = user
            }; 
            var post1 = new Post
            {
                Id = 2,
                Body = "It is test post 1!",
                UserId = 1,
                User = user
            }; var post2 = new Post
            {
                Id = 3,
                Body = "It is test post 2!",
                UserId = 1,
                User = user
            };

            using (var arrangeContext = new DBContext(options))
            {
                var sut = new PostService(arrangeContext, new FriendsService(arrangeContext));
                
                await arrangeContext.Countries.AddAsync(country);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.Posts.AddAsync(post);
                await arrangeContext.Posts.AddAsync(post1);
                //await arrangeContext.Posts.AddAsync(post2);
                //await sut.CreatePost(post.ToDTO());
                //await sut.CreatePost(post1.ToDTO());
                await sut.CreatePost(post2.ToDTO());
                await arrangeContext.SaveChangesAsync();
            }

            //Act
            using (var actContext = new DBContext(options))
            {
                var sut = new PostService(actContext, new FriendsService(actContext));
                //var list = (await actContext.Posts.FirstOrDefaultAsync(p => p.Id == 1)).User.FirstName;
                var result = await sut.GetUserPosts(1);

                //Assert
                Assert.AreEqual(result.Count(), 3);
            }
        }
    }
}
