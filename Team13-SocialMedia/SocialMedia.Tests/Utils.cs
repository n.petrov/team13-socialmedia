﻿using Microsoft.EntityFrameworkCore;
using SocialMedia.Database;

namespace SocialMedia.Tests
{
    public class Utils
    {
        public static DbContextOptions<DBContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<DBContext>()
                  .UseInMemoryDatabase(databaseName)
                  .Options;
        }
    }
}
