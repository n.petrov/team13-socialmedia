﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialMedia.Database;
using SocialMedia.Models;
using SocialMedia.Services;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.Tests.CountryServiceTests
{
    [TestClass]
    public class Delete_Country_Should
    {
        [TestMethod]
        public async Task ReturnTrue_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnTrue_When_ParamsAreValid));
           
            var country = new Country
            {
                Id = 1,
                Name = "Bulgaria"
            };

            using (var arrangeContext = new DBContext(options))
            {
                arrangeContext.Countries.Add(country);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new DBContext(options))
            {
                var sut = new CountryService(assertContext);

                //Act
                var result = await sut.Delete(1);
                var actual = await assertContext.Countries.FirstOrDefaultAsync(c => c.Id == 1);

                //Assert
                Assert.IsTrue(result);
                //Assert.IsTrue(actual.IsDeleted);
            }
        }

        [TestMethod]
        public async Task ReturnFalse_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnFalse_When_ParamsAreValid));

            //var providerMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new DBContext(options))
            {
                var sut = new CountryService(assertContext);

                //Act
                var result = await sut.Delete(1);

                //Assert
                Assert.IsFalse(result);
            }
        }

        [TestMethod]
        public async Task CorrectlySetDeleteValues_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(CorrectlySetDeleteValues_When_ParamsAreValid));

            var country = new Country
            {
                Id = 1,
                Name = "Bulgaria"
            };

            using (var arrangeContext = new DBContext(options))
            {
                arrangeContext.Countries.Add(country);
                arrangeContext.SaveChanges();
            }

            using (var actContext = new DBContext(options))
            {
                var sut = new CountryService(actContext);
                var result = await sut.Delete(1);
                Assert.IsTrue(result);
            }            
            
            using (var asertContext = new DBContext(options))
            {
                var sut = new CountryService(asertContext);
                var result = asertContext.Countries.FirstOrDefault(c => c.Id == 1);               
                Assert.IsNull(result);
            }           
        }
    }
}
