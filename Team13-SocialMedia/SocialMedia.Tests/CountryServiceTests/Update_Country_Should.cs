﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialMedia.Database;
using SocialMedia.Models;
using SocialMedia.Services;
using SocialMedia.Services.DTOs;
using System;
using System.Threading.Tasks;

namespace SocialMedia.Tests.CountryServiceTests
{
    [TestClass]
    public class Update_Comment_Should
    {
        [TestMethod]
        public async Task ReturnUpdatetCountryDTO_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnUpdatetCountryDTO_When_ParamsAreValid));
        
            var country = new Country
            {
                Id = 1,
                Name = "UK"
            };

            //Arange
            using (var arrangeContext = new DBContext(options))
            {
                arrangeContext.Countries.Add(country);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new DBContext(options))
            {
                var sut = new CountryService(actContext);
                var countryToUpdate = new CountryDTO
                {
                    Id = 1,
                    Name = "Bulgaria"
                };
                await sut.Update(1, countryToUpdate);
                actContext.SaveChanges();
            }
            //Asert
            using (var assertContext = new DBContext(options))
            {
                var sut = new CountryService(assertContext);
                var actual = await sut.Get(1);

                Assert.AreEqual(actual.Name, "Bulgaria");
            }
        }

        [TestMethod]
        public void Throw_When_BeerNotFound()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_BeerNotFound));

            //Act & Assert
            using (var actContext = new DBContext(options))
            {
                var sut = new CountryService(actContext);

                Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await sut.Get(1));
            }
        }
    }
}
