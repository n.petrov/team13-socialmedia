﻿//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using SocialMedia.Database;
//using SocialMedia.Models;
//using SocialMedia.Services;
//using System.Linq;
//using System.Threading.Tasks;

//namespace SocialMedia.Tests.CountryServiceTests
//{
//    [TestClass]
//    public class Get_Comment_Should
//    {
//        [TestMethod]
//        public async Task ReturnNewCountry_When_ParamsAreValid()
//        {
//            //Arrange
//            var options = Utils.GetOptions(nameof(ReturnNewCountry_When_ParamsAreValid));

//            var country = new Country
//            {
//                Id = 1,
//                Name = "UK"               
//            };           

//            using (var arrangeContext = new DBContext(options))
//            {
//                arrangeContext.Countries.Add(country);
//                arrangeContext.SaveChanges();
//            }

//            //Act
//            using (var actContext = new DBContext(options))
//            {
//                var sut = new CountryService(actContext);
//                var result = await sut.Get(1);

//                //Assert
//                Assert.AreEqual(country.Id, result.Id);
//                Assert.AreEqual(country.Name, result.Name);
//            }
//        }
//        [TestMethod]
//        public async Task ReturnNewCountries_When_ParamsAreValid()
//        {
//            //Arrange
//            var options = Utils.GetOptions(nameof(ReturnNewCountries_When_ParamsAreValid));

//            var country = new Country
//            {
//                Id = 1,
//                Name = "UK"
//            }; 
//            var country1 = new Country
//            {
//                Id = 2,
//                Name = "Belgium"
//            };
//            var country2 = new Country
//            {
//                Id = 3,
//                Name = "Bulgaria"
//            };

//            using (var arrangeContext = new DBContext(options))
//            {
//                arrangeContext.Countries.Add(country);
//                arrangeContext.Countries.Add(country1);
//                arrangeContext.Countries.Add(country2);
//                arrangeContext.SaveChanges();
//            }

//            //Act
//            using (var actContext = new DBContext(options))
//            {
//                var sut = new CountryService(actContext);
//                var result = await sut.GetMany();            

//                //Assert
//                Assert.AreEqual(3, result.Count());
//            }
//        }
//    }
//}
