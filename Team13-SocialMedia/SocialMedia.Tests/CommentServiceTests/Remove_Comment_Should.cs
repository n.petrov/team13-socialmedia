﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialMedia.Database;
using SocialMedia.Models;
using SocialMedia.Services;
using System;
using System.Threading.Tasks;

namespace SocialMedia.Tests.CommentServiceTests
{
    [TestClass]
    public class DeletePost_Should
    {
        [TestMethod]
        public async Task ReturnTrue_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnTrue_When_ParamsAreValid));

            var comment = new Comment
            {
                Id = 1,
                Body = "It is a test comment!",
                PostId = 1,
                UserId = 1
            };
            //var country = new Country
            //{
            //    Id = 1,
            //    Name = "Greece"

            //};
            //var user = new User
            //{
            //    Id = 1,
            //    FirstName = "Dwein",
            //    LastName = "Jonson",
            //    Age = 43,
            //    CreatedOn = DateTime.UtcNow,
            //    CountryId = 1,
            //    Nationality = country
            //};
          

            using (var arrangeContext = new DBContext(options))
            {
                await arrangeContext.Comments.AddAsync(comment);
               // await arrangeContext.Users.AddAsync(user);
              //  await arrangeContext.Countries.AddAsync(country);
                await arrangeContext.SaveChangesAsync();
            }

            //Act
            //using (var actContext = new DBContext(options))
            //{
            //    var sut = new CommentService(actContext);
            //    await sut.Remove(2);
            //    await actContext.SaveChangesAsync();
            //}

            using (var assertContext = new DBContext(options))
            {
                var sut = new CommentService(assertContext);
                var actual = await sut.Remove(1);

                Assert.IsTrue(actual);
            }
        }

        [TestMethod]
        public async Task ReturnFalse_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnFalse_When_ParamsAreValid));

            //var providerMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new DBContext(options))
            {
                var sut = new CommentService(assertContext);

                //Act
                var result = await sut.Remove(1);

                //Assert
                Assert.IsFalse(result);
            }
        }

        [TestMethod]
        public async Task CorrectlySetDeleteValues_When_ParamsAreValid()
        {
          //Arrange
            var options = Utils.GetOptions(nameof(CorrectlySetDeleteValues_When_ParamsAreValid));

            var comment = new Comment
            {
                Id = 1,
                Body = "It is a test comment!",
                PostId = 1,
                UserId = 1
            };

            using (var arrangeContext = new DBContext(options))
            {
                await arrangeContext.Comments.AddAsync(comment);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new DBContext(options))
            {
                var sut = new CommentService(actContext);
                await sut.Remove(1);
                await actContext.SaveChangesAsync();
            }

            using (var assertContext = new DBContext(options))
            {
                var sut = new CommentService(assertContext);

                var actual = await assertContext.Comments.FirstOrDefaultAsync(c => c.Id == 1);
                var dataTime = DateTime.Now;
                //Assert
                Assert.AreEqual(actual.Body, "[deleted]");
            }
        }
    }
}
