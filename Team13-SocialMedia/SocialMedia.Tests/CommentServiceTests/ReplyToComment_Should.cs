﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialMedia.Database;
using SocialMedia.Models;
using SocialMedia.Services;
using SocialMedia.Services.DTOs;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.Tests.CommentServiceTests
{
    [TestClass]
    public class GetFirstPost_Should
    {
        [TestMethod]
        public async Task ReturnCorrectlyValues_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(GetFirstPost_Should) + nameof(ReturnCorrectlyValues_When_ParamsAreValid));

            var country = new Country
            {
                Id = 1,
                Name = "Greece"

            };
            var user = new User
            {
                Id = 1,
                FirstName = "Dwein",
                LastName = "Jonson",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };
            var user2 = new User
            {
                Id = 2,
                FirstName = "John",
                LastName = "Doe",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };
            var comment = new Comment
            {
                Id = 1,
                Body = "It is a test comment!",
                PostId = 1,
                UserId = 1,
                User = user
            };

            using (var arrangeContext = new DBContext(options))
            {
                await arrangeContext.Comments.AddAsync(comment);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.Countries.AddAsync(country);
               // var sut = new CommentService(arrangeContext);
                //var actual = await sut.ReplyToComment(1, 1, body);
                await arrangeContext.SaveChangesAsync();

                //Assert.IsTrue(actual);
            } 
            // Act & Assert
            //using (var assertContext = new DBContext(options))
            //{
            //    //await assertContext.Comments.AddAsync(comment);
            //   
            //    var sut = new CommentService(assertContext);
            //    var comment1 = assertContext.Comments.FirstOrDefault(c => c.Id == 1);
            //    var actual = await sut.ReplyToComment(comment1.Id, 2, body);
            //    //await assertContext.SaveChangesAsync();
            //
            //    Assert.IsTrue(actual);
            //}

            //using (var assertContext = new DBContext(options))
            //{
            //    var sut = new CountryService(assertContext);

            //    //Act               
            //    var actual = assertContext.Comments.FirstOrDefault(c => c.UserId == 1 && c.PostId == 1);

            //    //Assert
            //    Assert.AreEqual("It is a test comment!", actual.Body);

            //}
        }
    }
}
