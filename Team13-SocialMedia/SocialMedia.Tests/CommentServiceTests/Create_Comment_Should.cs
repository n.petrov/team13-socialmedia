﻿//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using SocialMedia.Database;
//using SocialMedia.Services;
//using SocialMedia.Services.DTOs;
//using System;
//using System.Linq;
//using System.Threading.Tasks;

//namespace SocialMedia.Tests.CountryServiceTests
//{
//    [TestClass]
//    public class Create_Comment_Should
//    {
//        [TestMethod]
//        public async Task ReturnCountry_When_ParamsAreValid()
//        {
//            //Arrange
//            var options = Utils.GetOptions(nameof(ReturnCountry_When_ParamsAreValid));

//            var countryDTO = new CountryDTO
//            {
//                Id = 1,
//                Name = "Bulgaria"
//            };

//            using (var arrangeContext = new DBContext(options))
//            {
//                var sut = new CountryService(arrangeContext);
//                var newBeer = await sut.Create(countryDTO);
//                arrangeContext.SaveChanges();
//            }

//            using (var assertContext = new DBContext(options))
//            {
//                var sut = new CountryService(assertContext);

//                //Act               
//                var actual = assertContext.Countries.FirstOrDefault(b => b.Id == 1);

//                //Assert
//                Assert.AreEqual(countryDTO.Name, actual.Name);

//            }
//        }

//        [TestMethod]
//        public void Throw_When_CountryNotFound()
//        {
//            //Arrange
//            var options = Utils.GetOptions(nameof(Throw_When_CountryNotFound));

//            //Act & Assert
//            using (var actContext = new DBContext(options))
//            {
//                var sut = new CountryService(actContext);
//                var countryDTO = new CountryDTO();

//                Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await sut.Create(countryDTO));
//            }
//        }
//    }
//}
