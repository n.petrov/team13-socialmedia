﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialMedia.Database;
using SocialMedia.Models;
using SocialMedia.Services;
using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace SocialMedia.Tests.CommentServiceTests
{
    [TestClass]
    public class Edit_Post_Should
    {
        [TestMethod]
        public async Task ReturnUpdatetCommentDTO_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnUpdatetCommentDTO_When_ParamsAreValid));
        
            var comment = new Comment
            {
                Id = 1,
                Body = "It is a test comment!",
                PostId = 1,
                UserId = 1
            };

            //Arange
            using (var arrangeContext = new DBContext(options))
            {
                await arrangeContext.Comments.AddAsync(comment);
                await arrangeContext.SaveChangesAsync();
            }

            //Act
            using (var actContext = new DBContext(options))
            {
                var sut = new CommentService(actContext);

                var body = "It is the updated comment";
             
                await sut.Edit(1, body);
                await actContext.SaveChangesAsync();
            }
            //Asert
            using (var assertContext = new DBContext(options))
            {
                var sut = new CommentService(assertContext);
                //var actual = await sut.Get(1);
                var actual = await assertContext.Comments.FirstOrDefaultAsync(c => c.Id == 1);

                Assert.AreEqual(actual.Body, "It is the updated comment");
            }
        }

        [TestMethod]
        public void Throw_When_CommentNotFound()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_CommentNotFound));

            //Act & Assert
            using (var actContext = new DBContext(options))
            {
                Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await actContext.Comments
                                                                                               .FirstOrDefaultAsync(c => c.Id == 1));
            }
        }
    }
}
