﻿//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using SocialMedia.Database;
//using SocialMedia.Models;
//using SocialMedia.Services;
//using System;
//using System.Linq;
//using System.Threading.Tasks;

//namespace SocialMedia.Tests.CountryServiceTests
//{ 
//    [TestClass]
//    public class GetMeny_Comment_Should
//    {
//        [TestMethod]
//        public async Task ReturnNewCountris_When_ParamsAreValid()
//        {
//            //Arrange
//            var options = Utils.GetOptions(nameof(ReturnNewCountris_When_ParamsAreValid));

//            var country = new Country
//            {
//                Id = 1,
//                Name = "Bulgaria"
//            };
//            var country1 = new Country
//            {
//                Id = 2,
//                Name = "UK"
//            };
//            var country2 = new Country
//            {
//                Id = 3,
//                Name = "Belgium"
//            };           

//            using (var arrangeContext = new DBContext(options))
//            {
//                arrangeContext.Countries.Add(country);
//                arrangeContext.Countries.Add(country1);
//                arrangeContext.Countries.Add(country2);              
//                arrangeContext.SaveChanges();
//            }

//            //Act
//            using (var actContext = new DBContext(options))
//            {
//                var sut = new CountryService(actContext);
//                var result = await sut.GetMany();

//                //Assert
//                Assert.AreEqual(result.Count(), 3);
//            }
//        }

//        [TestMethod]
//        public void Throw_When_BrewerysNotExist()
//        {
//            //Arrange
//            var options = Utils.GetOptions(nameof(Throw_When_BrewerysNotExist));
//            //var providerMock = new Mock<IDateTimeProvider>();

//            //Act & Assert
//            using (var actContext = new DBContext(options))
//            {
//                var sut = new CountryService(actContext);

//                Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await sut.GetMany());
//            }
//        }
//    }
//}
