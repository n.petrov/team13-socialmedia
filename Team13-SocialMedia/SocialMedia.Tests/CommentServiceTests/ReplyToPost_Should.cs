﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialMedia.Database;
using SocialMedia.Services;
using SocialMedia.Services.DTOs;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.Tests.CommentServiceTests
{
    [TestClass]
    public class ReplyToPost_Should
    {
        [TestMethod]
        public async Task ReturnCorrectlyValues_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectlyValues_When_ParamsAreValid));

            string body = "It is a test comment!";

            using (var arrangeContext = new DBContext(options))
            {
                var sut = new CommentService(arrangeContext);
                await sut.ReplyToPost(1, 1, body);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new DBContext(options))
            {
                var sut = new CountryService(assertContext);

                //Act               
                var actual = assertContext.Comments.FirstOrDefault(c => c.UserId == 1 && c.PostId == 1);

                //Assert
                Assert.AreEqual("It is a test comment!", actual.Body);

            }
        }
    }
}
