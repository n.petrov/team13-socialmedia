﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialMedia.Database;
using SocialMedia.Models;
using SocialMedia.Services;
using System;
using System.Threading.Tasks;

namespace SocialMedia.Tests.FriendRequestServiceTests
{
    [TestClass]
    public class GetOutgoingRequests_Should
    {
        [TestMethod]
        public async Task Return_ListOfFriendRequest_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(GetOutgoingRequests_Should) 
                                         + nameof(Return_ListOfFriendRequest_When_ParamsAreValid));

            var country = new Country
            {
                Id = 1,
                Name = "Bulgaria"

            };
            var user = new User
            {
                Id = 1,
                FirstName = "Dwein",
                LastName = "Jonson",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };
            var user1 = new User
            {
                Id = 2,
                FirstName = "Will",
                LastName = "Smith",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };
            var user2 = new User
            {
                Id = 3,
                FirstName = "Andrea",
                LastName = "Bocelli",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };

            using (var arrangeContext = new DBContext(options))
            {
                await arrangeContext.Countries.AddAsync(country);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.Users.AddAsync(user1);
                await arrangeContext.Users.AddAsync(user2);

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new DBContext(options))
            {
                var sut = new FriendRequestService(actContext, new FriendsService(actContext));

                await sut.SendRequest(1, 3);
                await sut.SendRequest(1, 2);

                await actContext.SaveChangesAsync();
            }

            using (var assertContext = new DBContext(options))
            {
                var sut = new FriendRequestService(assertContext, new FriendsService(assertContext));

                var actual = await sut.GetOutgoingRequests(1);

                Assert.AreEqual(2, actual.Count);
            }
        }
    }
}
