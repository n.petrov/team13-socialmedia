﻿using Microsoft.AspNetCore.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SocialMedia.Database;
using SocialMedia.Models;
using SocialMedia.Services;
using SocialMedia.Services.Mapper;
using System;
using System.Threading.Tasks;

namespace SocialMedia.Tests.UserServiceTests
{
    [TestClass]
    public class Get_User_Should
    {
        [TestMethod]
        public async Task GetUser_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Get_User_Should) + nameof(GetUser_When_ParamsAreValid));

            var mockStore = new Mock<IUserStore<User>>();
            var userManager = new UserManager<User>(mockStore.Object, null, null, null, null, null, null, null, null);
           
            var country = new Country
            {
                Id = 1,
                Name = "Greece"

            };
            var user = new User
            {
                Id = 1,
                FirstName = "Dwein",
                LastName = "Jonson",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };
           

            using (var arrangeContext = new DBContext(options))
            {
                var sut = new UserService(arrangeContext, userManager, new RoleService(arrangeContext));
                await sut.Create(user.ToDTO());
                arrangeContext.Countries.Add(country);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new DBContext(options))
            {
                var sut = new UserService(assertContext, userManager, new RoleService(assertContext));
                
                //Act               
                var actual = await sut.Get(1);
                
                //Assert
                Assert.AreEqual("Dwein", actual.FirstName);
                Assert.AreEqual("Jonson", actual.LastName);
                Assert.AreEqual(43, actual.Age);
            }
        }

        [TestMethod]
        public async Task ReturnNull_When_UserNotFound()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Get_User_Should) + nameof(ReturnNull_When_UserNotFound));
            var mockStore = new Mock<IUserStore<User>>();
            var userManager = new UserManager<User>(mockStore.Object, null, null, null, null, null, null, null, null);

            //Act & Assert
            using (var actContext = new DBContext(options))
            {
                var sut = new UserService(actContext, userManager, new RoleService(actContext));
                var act = await sut.Get(1);

                Assert.IsNull(act);
            }
        }
    }
}
