﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SocialMedia.Database;
using SocialMedia.Models;
using SocialMedia.Services;
using SocialMedia.Services.Mapper;
using System;
using System.Threading.Tasks;

namespace SocialMedia.Tests.UserServiceTests
{
    [TestClass]
    public class Ban_User_Should
    {
        [TestMethod]
        public async Task UnbanUser_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Ban_User_Should) + nameof(UnbanUser_When_ParamsAreValid));

            var mockStore = new Mock<IUserStore<User>>();
            var userManager = new UserManager<User>(mockStore.Object, null, null, null, null, null, null, null, null);

            var country = new Country
            {
                Id = 1,
                Name = "Greece"

            };
            var user = new User
            {
                Id = 1,
                FirstName = "Dwein",
                LastName = "Jonson",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };

            using (var arrangeContext = new DBContext(options))
            {
                var sut = new UserService(arrangeContext, userManager, new RoleService(arrangeContext));
                var newUser = await sut.Create(user.ToDTO());
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new DBContext(options))
            {
                var sut = new UserService(assertContext, userManager, new RoleService(assertContext));
                await sut.Ban(1, "Because that's what I decided");

                //Act               
                var actual = await assertContext.Users.FirstOrDefaultAsync(u => u.Id == 1);

                //Assert
                Assert.AreEqual("Because that's what I decided", actual.BanDescription);

            }
        }

        [TestMethod]
        public async Task Throw_When_UserNotFound()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Ban_User_Should) + nameof(Throw_When_UserNotFound));
            var mockStore = new Mock<IUserStore<User>>();
            var userManager = new UserManager<User>(mockStore.Object, null, null, null, null, null, null, null, null);

            //Act & Assert
            using (var actContext = new DBContext(options))
            {
                var sut = new UserService(actContext, userManager, new RoleService(actContext));
                
                var actual = await sut.Ban(1, "ban desc");

                Assert.IsFalse(actual);
            }
        }
    }
}
