﻿using Microsoft.AspNetCore.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SocialMedia.Database;
using SocialMedia.Models;
using SocialMedia.Services;
using SocialMedia.Services.Mapper;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.Tests.UserServiceTests
{
    [TestClass]
    public class Unban_User_Should
    {
        [TestMethod]
        public async Task UnbanUser_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(UnbanUser_When_ParamsAreValid));

            var mockStore = new Mock<IUserStore<User>>();
            var userManager = new UserManager<User>(mockStore.Object, null, null, null, null, null, null, null, null);

            var country = new Country
            {
                Id = 1,
                Name = "Greece"

            };
            var user = new User
            {
                Id = 1,
                FirstName = "Dwein",
                LastName = "Jonson",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };

            using (var arrangeContext = new DBContext(options))
            {
                var sut = new UserService(arrangeContext, userManager, new RoleService(arrangeContext));
                var newUser = await sut.Create(user.ToDTO());
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new DBContext(options))
            {
                var sut = new UserService(assertContext, userManager, new RoleService(assertContext));
                await sut.Unban(1);

                //Act               
                var actual = assertContext.Users.FirstOrDefault(u => u.Id == 1);

                //Assert
                Assert.IsFalse(actual.LockoutEnabled);

            }
        }

        [TestMethod]
        public void Throw_When_UserNotFound()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_UserNotFound));
            var mockStore = new Mock<IUserStore<User>>();
            var userManager = new UserManager<User>(mockStore.Object, null, null, null, null, null, null, null, null);

            //Act & Assert
            using (var actContext = new DBContext(options))
            {
                var sut = new UserService(actContext, userManager, new RoleService(actContext));

                Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await sut.Unban(1));
            }
        }
    }
}
