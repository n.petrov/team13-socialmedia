﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SocialMedia.Database;
using SocialMedia.Models;
using SocialMedia.Services;
using SocialMedia.Services.DTOs;
using SocialMedia.Services.Mapper;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.Tests.UserServiceTests
{
    [TestClass]
    public class UpdatePrivacy_Should
    {
        [TestMethod]
        public async Task Return_True_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(UpdatePrivacy_Should) + nameof(Return_True_When_ParamsAreValid));
            
            var mockStore = new Mock<IUserStore<User>>();
            var userManager = new UserManager<User>(mockStore.Object, null, null, null, null, null, null, null, null);

            var country = new Country
            {
                Id = 1,
                Name = "Greece"
            };
            var user = new User
            {
                Id = 1,
                FirstName = "Dwein",
                LastName = "Jonson",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };
           
            using (var arrangeContext = new DBContext(options))
            {
                var sut = new UserService(arrangeContext, userManager, new RoleService(arrangeContext));
                await arrangeContext.Countries.AddAsync(country);
                await arrangeContext.Users.AddAsync(user);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new DBContext(options))
            {
                UserService sut = new UserService(actContext, userManager, new RoleService(actContext));
                await sut.UpdatePrivacy(1, 1);
                actContext.SaveChanges();
            }

            //Assert
            using (var assertContext = new DBContext(options))
            {
                UserService sut = new UserService(assertContext, userManager, new RoleService(assertContext));
                var actual = assertContext.Users
                                          .Include(s => s.Nationality)
                                          .FirstOrDefault(u => u.Id == 1);

                Assert.AreEqual(1, actual.PrivacySetting);
            }
        }

        [TestMethod]
        public async Task Return_False_When_UserNotFound()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(UpdatePrivacy_Should) + nameof(Return_False_When_UserNotFound));

            var mockStore = new Mock<IUserStore<User>>();
            var userManager = new UserManager<User>(mockStore.Object, null, null, null, null, null, null, null, null);

            var country = new Country
            {
                Id = 1,
                Name = "Greece"
            };
            var user = new User
            {
                Id = 1,
                FirstName = "Dwein",
                LastName = "Jonson",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };

            //Act & Assert
            using (var assertContext = new DBContext(options))
            {
                UserService sut = new UserService(assertContext, userManager, new RoleService(assertContext));
                var actual = await sut.UpdatePrivacy(1, 1);


                Assert.IsFalse(actual);
            }
        }

        [TestMethod]
        public async Task Return_False_When_PrivacyValueNotValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(UpdatePrivacy_Should) + nameof(Return_False_When_PrivacyValueNotValid));

            var mockStore = new Mock<IUserStore<User>>();
            var userManager = new UserManager<User>(mockStore.Object, null, null, null, null, null, null, null, null);

            var country = new Country
            {
                Id = 1,
                Name = "Greece"
            };
            var user = new User
            {
                Id = 1,
                FirstName = "Dwein",
                LastName = "Jonson",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };

            using (var arrangeContext = new DBContext(options))
            {
                var sut = new UserService(arrangeContext, userManager, new RoleService(arrangeContext));
                await arrangeContext.Countries.AddAsync(country);
                await arrangeContext.Users.AddAsync(user);
                arrangeContext.SaveChanges();
            }

            //Act & Assert
            using (var assertContext = new DBContext(options))
            {
                UserService sut = new UserService(assertContext, userManager, new RoleService(assertContext));
                var actual = await sut.UpdatePrivacy(1, 3);


                Assert.IsFalse(actual);
            }
        }
    }
}
