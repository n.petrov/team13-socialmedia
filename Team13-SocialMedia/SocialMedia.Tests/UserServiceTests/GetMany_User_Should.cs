﻿using Microsoft.AspNetCore.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SocialMedia.Database;
using SocialMedia.Models;
using SocialMedia.Services;
using System;
using System.Threading.Tasks;
using SocialMedia.Services.Mapper;
using System.Linq;

namespace SocialMedia.Tests.UserServiceTests
{
    [TestClass]
    public class GetMany_User_Should
    {
        /*[TestMethod]
        public async Task GetManyUser_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(GetManyUser_When_ParamsAreValid));

            var mockStore = new Mock<IUserStore<User>>();
            var userManager = new UserManager<User>(mockStore.Object, null, null, null, null, null, null, null, null);

            var country = new Country
            {
                Id = 1,
                Name = "Bulgaria"

            };
            var user = new User
            {
                Id = 1,
                FirstName = "Dwein",
                LastName = "Jonson",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1, 
                Nationality = country
            };
            var user1 = new User
            {
                Id = 2,
                FirstName = "Will",
                LastName = "Smith",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };
            var user2 = new User
            {
                Id = 3,
                FirstName = "Andrea",
                LastName = "Bocelli",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };

            using (var arrangeContext = new DBContext(options))
            {
                var sut = new UserService(arrangeContext, userManager, new RoleService(arrangeContext));
                await arrangeContext.Countries.AddAsync(country);
               // user.Nationality = country;
                await sut.Create(user.ToDTO());

                await sut.Create(user1.ToDTO());
                await sut.Create(user2.ToDTO());
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new DBContext(options))
            {
                var sut = new UserService(assertContext, userManager, new RoleService(assertContext));

                //Act               
                var actual = await sut.GetMany(null, null);

                //Assert
                Assert.AreEqual(3, actual.Count());
            }
        }*/

        [TestMethod]
        public void Throw_When_UsersNotFound()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_UsersNotFound));
            var mockStore = new Mock<IUserStore<User>>();
            var userManager = new UserManager<User>(mockStore.Object, null, null, null, null, null, null, null, null);

            //Act & Assert
            using (var actContext = new DBContext(options))
            {
                var sut = new UserService(actContext, userManager, new RoleService(actContext));

                Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await sut.GetMany(null, null));
            }
        }
    }
}
