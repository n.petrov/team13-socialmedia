﻿using Microsoft.AspNetCore.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SocialMedia.Database;
using SocialMedia.Models;
using SocialMedia.Services;
using SocialMedia.Services.DTOs;
using System;
using System.Threading.Tasks;
using SocialMedia.Services.Mapper;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace SocialMedia.Tests.UserServiceTests
{
    [TestClass]
    public class Update_User_Should
    {
        [TestMethod]
        public async Task ReturnUpdatedUser_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Update_User_Should) + nameof(ReturnUpdatedUser_When_ParamsAreValid));

            var mockStore = new Mock<IUserStore<User>>();
            var userManager = new UserManager<User>(mockStore.Object, null, null, null, null, null, null, null, null);
            var country = new Country
            {
                Id = 1,
                Name = "Greece"
            };

            var user = new User
            {
                Id = 1,
                FirstName = "Dwein",
                LastName = "Jonson",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };
            
            var userUpdateInfo = new UserDTO
            {
                Id = 1,
                FirstName = "Andrea",
                LastName = "Bocelli",
                Age = 62,
                CountryId = 1,
                Nationality = country.Name
            };

            using (var arrangeContext = new DBContext(options))
            {
                await arrangeContext.Countries.AddAsync(country);
                await arrangeContext.Users.AddAsync(user);
                
                await arrangeContext.SaveChangesAsync();
            }

            //Act
            using (var actContext = new DBContext(options))
            {
                UserService sut = new UserService(actContext, userManager, new RoleService(actContext));

                await sut.Update(1, userUpdateInfo);
                actContext.SaveChanges();
            }

            //Assert
            using (var assertContext = new DBContext(options))
            {
                UserService sut = new UserService(assertContext, userManager, new RoleService(assertContext));
                var actual = await assertContext.Users.Include(m => m.Nationality).FirstOrDefaultAsync(u => u.Id == 1);

                Assert.AreEqual("Andrea", actual.FirstName);
                Assert.AreEqual("Bocelli", actual.LastName);
                Assert.AreEqual(62, actual.Age);
            }
        }

        [TestMethod]
        public void Throw_When_UserNotFound()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Update_User_Should) + nameof(Throw_When_UserNotFound));
            var mockStore = new Mock<IUserStore<User>>();
            var userManager = new UserManager<User>(mockStore.Object, null, null, null, null, null, null, null, null);

            //Act & Assert
            using (var actContext = new DBContext(options))
            {
                var sut = new UserService(actContext, userManager, new RoleService(actContext));
                var userDTO = new UserDTO();

                Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await sut.Create(userDTO));
            }
        }
    }
}
