﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialMedia.Database;
using SocialMedia.Models;
using SocialMedia.Services;
using SocialMedia.Services.Mapper;
using System;
using System.Threading.Tasks;

namespace SocialMedia.Tests.FriendsServiceTest
{
    [TestClass]
    public class GetIncomingRequests_Should
    {
        /*[TestMethod]
        public async Task Return_AllFriends_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(GetIncomingRequests_Should) + nameof(Return_AllFriends_When_ParamsAreValid));

            var country = new Country
            {
                Id = 1,
                Name = "Bulgaria"

            };
            var user = new User
            {
                Id = 1,
                FirstName = "Dwein",
                LastName = "Jonson",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };
            var user1 = new User
            {
                Id = 2,
                FirstName = "Will",
                LastName = "Smith",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };
            var user2 = new User
            {
                Id = 3,
                FirstName = "Andrea",
                LastName = "Bocelli",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };

            using (var arrangeContext = new DBContext(options))
            {
                var sut = new FriendsService(arrangeContext);

                await arrangeContext.Countries.AddAsync(country);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.Users.AddAsync(user1);
                await arrangeContext.Users.AddAsync(user2);
                //await arrangeContext.FriendsList.AddAsync(new FriendsList { UserId = 1, FriendId = 3 });
                //await arrangeContext.FriendsList.AddAsync(new FriendsList { UserId = 1, FriendId = 2 });
                //await arrangeContext.FriendsList.AddAsync(friend);
                //await arrangeContext.FriendsList.AddAsync(friend2);
                await sut.AddFriend(2, 3);
                await sut.AddFriend(2, 1);

                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new DBContext(options))
            {
                var sut = new FriendsService(actContext);
                await sut.AddFriend(2, 3);
                await sut.AddFriend(2, 1);

                await actContext.SaveChangesAsync();
            } 
            
            using (var assertContext = new DBContext(options))
            {
                var sut = new FriendsService(assertContext);

                //Act               
                var actual = await sut.GetFriends(2);

                //Assert
                Assert.AreEqual(2, actual.Count);
            }
        }*/

        [TestMethod]
        public async Task ReturnFalse_When_UserIsNull()
        {

            //Arrange
            var options = Utils.GetOptions(nameof(GetIncomingRequests_Should) + nameof(ReturnFalse_When_UserIsNull));

            var country = new Country
            {
                Id = 1,
                Name = "Bulgaria"

            };
            var user = new User
            {
                Id = 1,
                FirstName = "Dwein",
                LastName = "Jonson",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };
            var user1 = new User
            {
                Id = 2,
                FirstName = "Will",
                LastName = "Smith",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };
            var user2 = new User
            {
                Id = 3,
                FirstName = "Andrea",
                LastName = "Bocelli",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };

            using (var arrangeContext = new DBContext(options))
            {
                await arrangeContext.Countries.AddAsync(country);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.Users.AddAsync(user1);
                //await arrangeContext.Users.AddAsync(user2);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new DBContext(options))
            {
                var sut = new FriendsService(assertContext);

                //Act               
                var actual = await sut.AddFriend(1, 3);

                //Assert
                Assert.IsFalse(actual);
            }
        }

        [TestMethod]
        public async Task ReturnFalse_When_FriendIsNull()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(GetIncomingRequests_Should) + nameof(ReturnFalse_When_FriendIsNull));

            var country = new Country
            {
                Id = 1,
                Name = "Bulgaria"

            };
            var user = new User
            {
                Id = 1,
                FirstName = "Dwein",
                LastName = "Jonson",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };
            var user1 = new User
            {
                Id = 2,
                FirstName = "Will",
                LastName = "Smith",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };
            var user2 = new User
            {
                Id = 3,
                FirstName = "Andrea",
                LastName = "Bocelli",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };

            using (var arrangeContext = new DBContext(options))
            {
                await arrangeContext.Countries.AddAsync(country);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.Users.AddAsync(user1);
                //await arrangeContext.Users.AddAsync(user2);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new DBContext(options))
            {
                var sut = new FriendsService(assertContext);

                //Act               
                var actual = await sut.AddFriend(3, 1);

                //Assert
                Assert.IsFalse(actual);
            }
        }
    }
}
