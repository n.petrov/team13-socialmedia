﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialMedia.Database;
using SocialMedia.Models;
using SocialMedia.Services;
using System;
using System.Threading.Tasks;

namespace SocialMedia.Tests.FriendsServiceTest
{
    [TestClass]
    public class DeleteFriend_Should
    {
        [TestMethod]
        public async Task Return_True_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(DeleteFriend_Should) + nameof(Return_True_When_ParamsAreValid));

            var country = new Country
            {
                Id = 1,
                Name = "Bulgaria"

            };
            var user = new User
            {
                Id = 1,
                FirstName = "Dwein",
                LastName = "Jonson",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };
            var user1 = new User
            {
                Id = 2,
                FirstName = "Will",
                LastName = "Smith",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };
            var user2 = new User
            {
                Id = 3,
                FirstName = "Andrea",
                LastName = "Bocelli",
                Age = 43,
                CreatedOn = DateTime.UtcNow,
                CountryId = 1,
                Nationality = country
            };
            FriendsList friend = new FriendsList
            {
                UserId = 1,
                FriendId = 3
            };
            //TODO: FIX friend relation bodge.
            FriendsList friend2 = new FriendsList
            {
                UserId = 3,
                FriendId = 1
            };

            using (var arrangeContext = new DBContext(options))
            {
                var sut = new FriendsService(arrangeContext);

                await arrangeContext.Countries.AddAsync(country);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.Users.AddAsync(user1);
                await arrangeContext.Users.AddAsync(user2);
                
                await arrangeContext.SaveChangesAsync();
            }

            //Act
            using (var actContext = new DBContext(options))
            {
                var sut = new FriendsService(actContext);

                await sut.AddFriend(1, 3);
                await actContext.SaveChangesAsync();
            }

            //Assert
            using (var assertContext = new DBContext(options))
            {
                var sut = new FriendsService(assertContext);
      
                var actual = await sut.DeleteFriend(1, 3);

                Assert.IsTrue(actual);
            }
        }

        [TestMethod]
        public async Task ReturnFalse_When_ParamsAreValid()
        {

            //Arrange
            var options = Utils.GetOptions(nameof(DeleteFriend_Should) + nameof(ReturnFalse_When_ParamsAreValid));

            using (var assertContext = new DBContext(options))
            {
                var sut = new FriendsService(assertContext);
                var actual = await sut.DeleteFriend(1, 3);

                Assert.IsFalse(actual);
            }
        }
    }
}
