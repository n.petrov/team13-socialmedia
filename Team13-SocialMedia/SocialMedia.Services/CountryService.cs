﻿using Microsoft.EntityFrameworkCore;
using SocialMedia.Database;
using SocialMedia.Models;
using SocialMedia.Services.Contracts;
using SocialMedia.Services.DTOs;
using SocialMedia.Services.Mapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.Services
{
    public class CountryService : ICountryService
    {
        private readonly DBContext _dbContext;

        public CountryService(DBContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<CountryDTO> Create(CountryDTO countryDTO)
        {
            if (countryDTO == null)
                return null;

            if (await _dbContext.Countries.AnyAsync(x => x.Name == countryDTO.Name))
                return null;

            Country country = countryDTO.ToModel();

            await _dbContext.Countries.AddAsync(country);
            await _dbContext.SaveChangesAsync();

            return countryDTO;
        }

        public async Task<bool> Delete(int id)
        {
            Country country = await _dbContext.Countries
                .FirstOrDefaultAsync(c => c.Id == id);

            if (country == null)
            {
                return false;
            }

            _dbContext.Countries.Remove(country);

            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<CountryDTO> Get(int id)
        {
            Country country = await _dbContext.Countries
                .FirstOrDefaultAsync(c => c.Id == id);

            if (country == null)
                return null;

            CountryDTO countryDTO = country.ToDTO();

            return countryDTO;
        }

        public async Task<ICollection<CountryDTO>> GetMany()
        {
            ICollection<CountryDTO> countryList = await _dbContext.Countries
                .Select(c => c.ToDTO())
                .ToListAsync();

            return countryList;
        }

        public async Task<CountryDTO> Update(int id, CountryDTO countryDTO)
        {
            if (countryDTO == null)
                return null;

            Country country = await _dbContext.Countries
                .FirstOrDefaultAsync(c => c.Id == id);

            if (country == null)
                return null;

            country.Id = countryDTO.Id;
            country.Name = countryDTO.Name;

            await _dbContext.SaveChangesAsync();

            return countryDTO;
        }
    }
}
