﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using SocialMedia.Database;
using SocialMedia.Models;
using SocialMedia.Services.Contracts;
using SocialMedia.Services.DTOs;
using SocialMedia.Services.Mapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.Services
{
    public class UserService : IUserService
    {
        private readonly DBContext _dbContext;
        private readonly IRoleService _roleService;
        private readonly UserManager<User> _userManager;

        public UserService(DBContext dbContext, UserManager<User> userManager, IRoleService roleService)
        {
            _dbContext = dbContext;
            _userManager = userManager;
            _roleService = roleService;
        }

        public async Task<UserDTO> Create(UserDTO userDTO)
        {
            User toAdd = userDTO.ToModel();
            toAdd.CreatedOn = DateTime.Now;

            await _dbContext.Users.AddAsync(toAdd);
            await _dbContext.SaveChangesAsync();

            return userDTO;
        }

        private enum SortString
        {
            Id, FirstName, LastName, UserName, Age, Role
        }

        public async Task<UserDTO> Get(int id)
        {
            User user = await _dbContext.Users
                .Include(x => x.Nationality)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (user == null)
                return null;

            return user.ToDTO();
        }

        public async Task<IEnumerable<UserDTO>> GetMany(string sortBy = "Id", string order = "asc", string searchQuery = "")
        {
            IEnumerable<UserDTO> userList;

            string orderSanitizedString = (order != "desc") ? "" : " DESC";

            Enum.TryParse(sortBy, out SortString sortSanitizedString);

            userList = (await _dbContext.Users
                .FromSqlRaw("SELECT TOP (100) PERCENT * FROM AspNetUsers ORDER BY " + sortSanitizedString.ToString() + orderSanitizedString)
                .Include(x => x.Nationality)
                .ToListAsync())
                .Select(x => x.ToDTO())
                .Where(x => $"{x.FirstName} {x.LastName}".Contains(searchQuery, StringComparison.OrdinalIgnoreCase));

            if (userList == null)
            {
                throw new ArgumentNullException();
            }

            return userList;
        }

        public async Task<bool> RemoveFromDb(int id)
        {
            User user = await _dbContext.Users
                //.Where(x => x.IsDeleted == false)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (user == null)
            {
                return false;
            }

            _dbContext.Users.Remove(user);
            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> Ban(int id, string description)
        {
            User toBan = await _dbContext.Users
                .FirstOrDefaultAsync(x => x.Id == id);

            if (toBan == null)
            {
                return false;
            }

            toBan.LockoutEnabled = true;
            toBan.LockoutEnd = DateTime.Now.AddYears(100);
            toBan.BanDescription = description;
            toBan.BannedOn = DateTime.Now;

            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> Unban(int id)
        {
            User user = await _dbContext.Users
                .FirstOrDefaultAsync(x => x.Id == id);

            if (user == null)
            {
                return false;
            }

            user.LockoutEnabled = false;
            user.UnbannedOn = DateTime.Now;

            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> Update(int id, UserDTO userDTO)
        {
            User toUpdate = await _dbContext.Users
                .FirstOrDefaultAsync(x => x.Id == id);

            if (toUpdate == null)
            {
                return false;
            }

            toUpdate.UserName = userDTO.Email;
            toUpdate.Email = userDTO.Email;
            toUpdate.FirstName = userDTO.FirstName;
            toUpdate.LastName = userDTO.LastName;
            toUpdate.PhoneNumber = userDTO.PhoneNumber;
            toUpdate.About = userDTO.About;
            toUpdate.CountryId = userDTO.CountryId;
            toUpdate.DateOfBirth = userDTO.DateOfBirth;
            toUpdate.Age = userDTO.Age;
            toUpdate.EditedOn = DateTime.Now;

            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> UpdatePrivacy(int userId, byte privacySetting)
        {
            User toUpdate = await _dbContext.Users
                .FirstOrDefaultAsync(x => x.Id == userId);

            if (toUpdate == null)
                return false;

            if (privacySetting < 0 || privacySetting > 2)
                return false;

            toUpdate.PrivacySetting = privacySetting;
            toUpdate.EditedOn = DateTime.Now;

            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> ChangePassword(int userId, string oldPassword, string newPassword)
        {
            User user = await _dbContext.Users
                .FirstOrDefaultAsync(x => x.Id == userId);

            if (user == null)
                return false;

            PasswordHasher<User> hasher = new PasswordHasher<User>();

            user.PasswordHash = hasher.HashPassword(user, newPassword);
            user.SecurityStamp = Guid.NewGuid().ToString();

            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> ChangeRole(int userId, string role)
        {
            User user = await _dbContext.Users
                .FirstOrDefaultAsync(x => x.Id == userId);

            if (user == null)
                return false;

            await _userManager.RemoveFromRoleAsync(user, await _roleService.GetRole(userId));
            await _userManager.AddToRoleAsync(user, role);

            return true;
        }

        public async Task<ICollection<UserDTO>> GetPublicUsers(string sortBy = "Id", string order = "asc")
        {
            ICollection<UserDTO> userList;

            string orderSanitizedString = (order != "desc") ? "" : " DESC";

            Enum.TryParse(sortBy, out SortString sortSanitizedString);

            userList = await _dbContext.Users
                .FromSqlRaw("SELECT * FROM AspNetUsers ORDER BY " + sortSanitizedString.ToString() + orderSanitizedString)
                .Include(x => x.Nationality)
                .Where(x => x.PrivacySetting == 2)
                .Select(x => x.ToDTO())
                .ToListAsync();

            if (userList == null)
            {
                throw new ArgumentNullException();
            }

            return userList;
        }
    }
}
