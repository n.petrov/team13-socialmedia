﻿using SocialMedia.Models;
using SocialMedia.Services.DTOs;

namespace SocialMedia.Services.Mapper
{
    public static class ModelMapper
    {
        public static Post ToModel(this PostDTO model)
        {
            return new Post
            {
                Id = model.Id,
                UserId = model.UserId,
                Title = model.Title,
                Body = model.Body,
                Picture = model.Picture,
                PictureType = model.PictureType
            };
        }

        public static Country ToModel(this CountryDTO model)
        {
            return new Country
            {
                Id = model.Id,
                Name = model.Name
            };
        }

        public static User ToModel(this UserDTO model)
        {
            return new User
            {
                Id = model.Id,
                CountryId = model.CountryId,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                Age = model.Age,
                Address = model.Address,
                DateOfBirth = model.DateOfBirth,
                JobDescription = model.JobDescription,
                PhoneNumber = model.PhoneNumber,
                About = model.About,
                PrivacySetting = model.PrivacySetting
            };
        }

        public static FriendsList ToModel(this FriendsListDTO model)
        {
            return new FriendsList
            {
                UserId = model.UserId,
                FriendId = model.FriendId
            };
        }

        public static FavoritesList ToModel(this FavoritesListDTO model)
        {
            return new FavoritesList
            {
                UserId = model.UserId,
                PostId = model.PostId
            };
        }

        public static Comment ToModel(this CommentDTO model)
        {
            return new Comment
            {
                Id = model.Id,
                UserId = model.UserId,
                Body = model.Body
            };
        }
    }
}
