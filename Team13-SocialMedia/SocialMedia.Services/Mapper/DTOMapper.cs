﻿using SocialMedia.Models;
using SocialMedia.Services.DTOs;
using System.Linq;

namespace SocialMedia.Services.Mapper
{
    public static class DTOMapper
    {
        public static PostDTO ToDTO(this Post model)
        {
            return new PostDTO
            {
                Id = model.Id,
                UserId = model.UserId,
                Title = model.Title,
                Body = model.Body,
                CreatedOn = model.CreatedOn,
                UserFirstName = model.User.FirstName,
                UserLastName = model.User.LastName,
                Picture = model.Picture,
                PictureType = model.PictureType,
                Comments = model.Comments.Select(x => x.ToDTO()).ToList(),
                Likes = model.Likes.Count,
                Dislikes = model.Dislikes.Count
            };
        }

        public static UserDTO ToDTO(this User model)
        {
            return new UserDTO
            {
                Id = model.Id,
                CountryId = model.CountryId,
                Nationality = model.Nationality.Name,
                FirstName = model.FirstName,
                LastName = model.LastName,
                UserName = model.UserName,
                Email = model.Email,
                IsBanned = model.LockoutEnabled,
                BannedOn = model.BannedOn,
                BanDescription = model.BanDescription,
                JobDescription = model.JobDescription,
                Address = model.Address,
                Age = model.Age,
                DateOfBirth = model.DateOfBirth,
                PrivacySetting = model.PrivacySetting,
                PhoneNumber = model.PhoneNumber,
                About = model.About,
                ProfilePicture = model.ProfilePicture,
                PictureType = model.PictureType
            };
        }

        public static FriendRequestDTO ToDTO(this FriendRequest model)
        {
            return new FriendRequestDTO
            {
                Id = model.Id,
                SenderId = model.SenderId,
                ReceiverId = model.ReceiverId,
                CreatedOn = model.CreatedOn,
                SenderFullName = $"{model.Sender.FirstName} {model.Sender.LastName}",
                SenderPfp = model.Sender.ProfilePicture,
                ReceiverFullName = $"{model.Receiver.FirstName} {model.Receiver.LastName}",
                ReceiverPfp = model.Receiver.ProfilePicture,
                RequestBody = model.RequestBody
            };
        }

        public static FriendsListDTO ToDTO(this FriendsList model)
        {
            return new FriendsListDTO
            {
                UserId = model.UserId,
                FriendId = model.FriendId
            };
        }

        public static FavoritesListDTO ToDTO(this FavoritesList model)
        {
            return new FavoritesListDTO
            {
                UserId = model.UserId,
                PostId = model.PostId
            };
        }


        public static CountryDTO ToDTO(this Country model)
        {
            return new CountryDTO
            {
                Id = model.Id,
                Name = model.Name
            };
        }

        public static CommentDTO ToDTO(this Comment model)
        {
            return new CommentDTO
            {
                Id = model.Id,
                UserId = (model.User == null) ? -1 : model.UserId,
                PostId = model.PostId,
                Body = model.Body,
                CommenterName = (model.User == null) ? "[deleted]" : $"{model.User.FirstName} {model.User.LastName}",
                CommenterPfp = (model.User == null) ? new byte[1] : model.User.ProfilePicture,
                Likes = model.Likes.Count,
                Dislikes = model.Dislikes.Count,
                CreatedOn = model.CreatedOn,
                EditedOn = model.EditedOn
            };
        }
    }
}
