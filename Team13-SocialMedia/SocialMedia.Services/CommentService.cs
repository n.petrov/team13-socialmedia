﻿using Microsoft.EntityFrameworkCore;
using SocialMedia.Database;
using SocialMedia.Models;
using SocialMedia.Services.Contracts;
using SocialMedia.Services.DTOs;
using SocialMedia.Services.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.Services
{
    public class CommentService : ICommentService
    {
        private readonly DBContext _dbContext;

        public CommentService(DBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<string> ReplyToPost(int postId, int userId, string body)
        {
            await _dbContext.Comments.AddAsync(
                new Comment
                {
                    Body = body,
                    UserId = userId,
                    PostId = postId,
                    CreatedOn = DateTime.Now
                });

            await _dbContext.SaveChangesAsync();

            return body;
        }

        public async Task<bool> Edit(int commentId, string body)
        {
            Comment comment = await _dbContext.Comments
                .FirstOrDefaultAsync(x => x.Id == commentId);

            if (comment == null)
            {
                return false;
            }

            comment.Body = body;

            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> Remove(int commentId)
        {
            Comment comment = await _dbContext.Comments
                .FirstOrDefaultAsync(x => x.Id == commentId);

            if (comment == null)
            {
                return false;
            }

            this.RemoveAllRatings(commentId);

            comment.DeletedOn = DateTime.Now;
            comment.Body = "[deleted]";
            comment.User = null;
            comment.UserId = null;

            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<ICollection<CommentDTO>> GetPostComments(int postId)
        {
            return await _dbContext.Comments
                .Where(x => x.PostId == postId)
                .Include(x => x.User)
                .Include(x => x.Likes)
                .Include(x => x.Dislikes)
                .Select(x => x.ToDTO())
                .ToListAsync();
        }

        private void RemoveAllRatings(int commentId)
        {
            foreach (CommentLike like in _dbContext.CommentLikes.Where(x => x.CommentId == commentId))
            {
                _dbContext.Remove(like);
            }

            foreach (CommentDislike dislike in _dbContext.CommentDislikes.Where(x => x.CommentId == commentId))
            {
                _dbContext.Remove(dislike);
            }
        }
    }
}