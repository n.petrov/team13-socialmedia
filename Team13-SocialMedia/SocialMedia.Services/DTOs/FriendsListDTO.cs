﻿namespace SocialMedia.Services.DTOs
{
    public class FriendsListDTO
    {
        public int? UserId { get; set; }
        public int? FriendId { get; set; }
    }
}
