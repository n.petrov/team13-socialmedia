﻿using System;

namespace SocialMedia.Services.DTOs
{
    public class PostPictureDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FileType { get; set; }
        public string Extension { get; set; }
        public DateTime CreatedOn { get; set; }
        public byte[] Data { get; set; }
        public int? UploaderId { get; set; }
        public int? PostId { get; set; }
    }
}
