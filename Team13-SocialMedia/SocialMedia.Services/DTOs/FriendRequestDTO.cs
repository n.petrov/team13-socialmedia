﻿using System;

namespace SocialMedia.Services.DTOs
{
    public class FriendRequestDTO
    {
        public int Id { get; set; }
        public int? SenderId { get; set; }
        public int? ReceiverId { get; set; }
        public byte[] SenderPfp { get; set; }
        public string SenderFullName { get; set; }
        public byte[] ReceiverPfp { get; set; }
        public string ReceiverFullName { get; set; }
        public string RequestBody { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
