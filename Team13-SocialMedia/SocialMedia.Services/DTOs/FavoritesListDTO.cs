﻿namespace SocialMedia.Services.DTOs
{
    public class FavoritesListDTO
    {
        public int UserId { get; set; }
        public int PostId { get; set; }
    }
}
