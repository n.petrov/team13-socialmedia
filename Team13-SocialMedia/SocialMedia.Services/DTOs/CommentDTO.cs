﻿using System;
using System.Collections.Generic;

namespace SocialMedia.Services.DTOs
{
    public class CommentDTO
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? PostId { get; set; }
        public string Body { get; set; }
        public string CommenterName { get; set; }
        public byte[] CommenterPfp { get; set; }
        public int Likes { get; set; }
        public int Dislikes { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime EditedOn { get; set; }
        public ICollection<CommentDTO> Replies { get; set; }
    }
}
