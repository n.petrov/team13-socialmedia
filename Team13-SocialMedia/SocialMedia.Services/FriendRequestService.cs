﻿using Microsoft.EntityFrameworkCore;
using SocialMedia.Database;
using SocialMedia.Models;
using SocialMedia.Services.Contracts;
using SocialMedia.Services.DTOs;
using SocialMedia.Services.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.Services
{
    public class FriendRequestService : IFriendRequestService
    {
        private readonly DBContext _dbContext;
        private readonly IFriendsService _friendsService;

        public FriendRequestService(DBContext dbContext, IFriendsService friendsService)
        {
            _dbContext = dbContext;
            _friendsService = friendsService;
        }

        public async Task<byte> SendRequest(int senderId, int receiverId, string requestBody = "I would like to be your friend!")
        {
            if (await _friendsService.IsFriendsWith(senderId, receiverId))
                return 3;

            if (await _dbContext.FriendRequests.
                AnyAsync(x => x.ReceiverId == receiverId && x.SenderId == senderId))
            {
                return 0;
            }

            if (await _dbContext.FriendRequests.
                AnyAsync(x => x.ReceiverId == senderId && x.SenderId == receiverId))
            {
                return 1;
            }

            await _dbContext.AddAsync(
                new FriendRequest
                {
                    SenderId = senderId,
                    ReceiverId = receiverId,
                    RequestBody = requestBody,
                    CreatedOn = DateTime.Now
                });

            await _dbContext.SaveChangesAsync();

            return 2;
        }

        public async Task<bool> AcceptRequest(int requestId)
        {
            FriendRequest request = await _dbContext.FriendRequests
                .FirstOrDefaultAsync(x => x.Id == requestId);

            if (request == null)
                return false;

            int userId = (int)request.SenderId;
            int friendId = (int)request.ReceiverId;

            if (!await _friendsService.AddFriend(userId, friendId))
                return false;

            _dbContext.Remove(request);
            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> RemoveRequest(int requestId)
        {
            FriendRequest request = await _dbContext.FriendRequests
                .FirstOrDefaultAsync(x => x.Id == requestId);

            if (request == null)
                return false;

            _dbContext.Remove(request);
            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<ICollection<FriendRequestDTO>> GetIncomingRequests(int userId)
        {
            return await _dbContext.FriendRequests
                .Where(x => x.ReceiverId == userId)
                .Include(x => x.Sender)
                .Include(x => x.Receiver)
                .Select(x => x.ToDTO())
                .ToListAsync();
        }

        public async Task<ICollection<FriendRequestDTO>> GetOutgoingRequests(int userId)
        {
            return await _dbContext.FriendRequests
                .Where(x => x.SenderId == userId)
                .Include(x => x.Sender)
                .Include(x => x.Receiver)
                .Select(x => x.ToDTO())
                .ToListAsync();
        }
    }
}
