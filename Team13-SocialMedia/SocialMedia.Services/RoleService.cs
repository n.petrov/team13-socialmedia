﻿using Microsoft.EntityFrameworkCore;
using SocialMedia.Database;
using SocialMedia.Services.Contracts;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.Services
{
    public class RoleService : IRoleService
    {
        private readonly DBContext _dbContext;

        public RoleService(DBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<string> GetRole(int userId)
        {
            return (await _dbContext.Roles
                .FromSqlRaw("SELECT * FROM AspNetRoles " +
                            "WHERE Id IN " +
                            "(SELECT RoleId FROM AspNetUserRoles " +
                            "WHERE UserId IN " +
                            "(SELECT Id FROM AspNetUsers " +
                            "WHERE Id = {0}))", userId)
                .FirstOrDefaultAsync())
                .Name;
        }

        public async Task<List<string>> GetAllRoles()
        {
            return await _dbContext.Roles
                .Select(x => x.Name)
                .ToListAsync();
        }
    }
}
