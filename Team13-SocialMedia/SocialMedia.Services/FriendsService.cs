﻿using Microsoft.EntityFrameworkCore;
using SocialMedia.Database;
using SocialMedia.Models;
using SocialMedia.Services.Contracts;
using SocialMedia.Services.DTOs;
using SocialMedia.Services.Mapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.Services
{
    public class FriendsService : IFriendsService
    {
        private readonly DBContext _dbContext;

        public FriendsService(DBContext dbcontext)
        {
            _dbContext = dbcontext;
        }

        public async Task<bool> AddFriend(int userId, int friendId)
        {
            if (userId == friendId)
                return false;

            if (!await _dbContext.Users.AnyAsync(x => x.Id == userId))
                return false;

            if (!await _dbContext.Users.AnyAsync(x => x.Id == friendId))
                return false;

            if (await IsFriendsWith(userId, friendId))
                return false;

            FriendsList friend1 = new FriendsList
            {
                UserId = userId,
                FriendId = friendId
            };

            FriendsList friend2 = new FriendsList
            {
                UserId = friendId,
                FriendId = userId
            };

            await _dbContext.FriendsList.AddAsync(friend1);
            await _dbContext.FriendsList.AddAsync(friend2);
            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DeleteFriend(int userId, int friendId)
        {
            FriendsList friend1 = await _dbContext.FriendsList
                .FirstOrDefaultAsync(x => x.UserId == userId && x.FriendId == friendId);

            FriendsList friend2 = await _dbContext.FriendsList
                .FirstOrDefaultAsync(x => x.UserId == friendId && x.FriendId == userId);

            if (friend1 == null)
                return false;

            if (friend2 == null)
                return false;

            _dbContext.FriendsList.Remove(friend1);
            _dbContext.FriendsList.Remove(friend2);
            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<ICollection<UserDTO>> GetFriends(int userId)
        {
            return await _dbContext.Users
                .FromSqlRaw("SELECT * FROM AspNetUsers " +
                            "WHERE Id IN " +
                            "(SELECT FriendId FROM FriendsList " +
                            "WHERE UserId = {0})", userId)
                .Include(x => x.Nationality)
                .Select(x => x.ToDTO())
                .ToListAsync();
        }

        public async Task<bool> IsFriendsWith(int user1Id, int user2Id)
        {
            return await _dbContext.FriendsList.AnyAsync(x =>
                x.UserId == user1Id && x.FriendId == user2Id);
        }
    }
}
