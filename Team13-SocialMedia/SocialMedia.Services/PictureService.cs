﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using SocialMedia.Database;
using SocialMedia.Models;
using SocialMedia.Services.Contracts;
using System;
using System.IO;
using System.Threading.Tasks;

namespace SocialMedia.Services
{
    public class PictureService : IPictureService
    {
        private readonly DBContext _dbContext;

        public PictureService(DBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> EditPostPicture(int postId, IFormFile file)
        {
            Post toEdit = await _dbContext.Posts
                .FirstOrDefaultAsync(x => x.Id == postId);

            if (toEdit == null)
                return false;

            if (file == null)
                return false;

            byte[] data;

            using (MemoryStream stream = new MemoryStream())
            {
                await file.CopyToAsync(stream);
                data = stream.ToArray();
            }

            toEdit.Picture = data;
            toEdit.PictureType = file.ContentType;

            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> EditProfilePicture(int userId, IFormFile file)
        {
            User toEdit = await _dbContext.Users
                .FirstOrDefaultAsync(x => x.Id == userId);

            if (toEdit == null)
                return false;

            if (file == null)
                return false;

            byte[] data;

            using (MemoryStream stream = new MemoryStream())
            {
                await file.CopyToAsync(stream);
                data = stream.ToArray();
            }

            toEdit.ProfilePicture = data;
            toEdit.PictureType = file.ContentType;

            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<(byte[] Data, string DataType)> GetFileData(IFormFile file)
        {
            (byte[] Data, string DataType) data = (new byte[1], "image/png");

            if (file == null)
                return data;

            using (MemoryStream stream = new MemoryStream())
            {
                await file.CopyToAsync(stream);
                data.Data = stream.ToArray();
            }

            data.DataType = file.ContentType;

            return data;
        }

        #region Validation
        /// <summary>
        /// Validates inputs.
        /// </summary>
        /// <returns>false if model is null or if user does not exist.</returns>
        private async Task<bool> IsValidUser(int userId, IFormFile file)
        {
            if (file == null)
                return false;

            if (!await _dbContext.Users.AnyAsync(x => x.Id == userId))
                return false;

            return true;
        }

        /// <summary>
        /// Validates inputs.
        /// </summary>
        /// <returns>false if model is null or if post does not exist.</returns>
        private async Task<bool> IsValidPost(int postId, IFormFile file)
        {
            if (file == null)
                return false;

            if (!await _dbContext.Posts.AnyAsync(x => x.Id == postId))
                return false;

            return true;
        }
        #endregion
    }
}
