﻿using Microsoft.EntityFrameworkCore;
using SocialMedia.Database;
using SocialMedia.Models;
using SocialMedia.Services.Contracts;
using SocialMedia.Services.DTOs;
using SocialMedia.Services.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.Services
{
    public class PostService : IPostService
    {
        private readonly DBContext _dbContext;
        private readonly IFriendsService _friendsService;

        public PostService(DBContext dBContext, IFriendsService friendsService)
        {
            _dbContext = dBContext;
            _friendsService = friendsService;
        }

        public async Task<ICollection<PostDTO>> GetAll()
        {
            return await _dbContext.Posts
                .Include(x => x.User)
                .Include(x => x.Comments)
                    .ThenInclude(c => c.User)
                .Include(x => x.Likes)
                .Include(x => x.Dislikes)
                .Select(x => x.ToDTO())
                .ToListAsync();
        }

        //SELECT Posts.*, counter.Count
        //FROM Posts
        //LEFT JOIN(
        //	SELECT PostLikes.PostId, COUNT(PostLikes.PostId) as Count
        //	FROM PostLikes
        //	GROUP BY PostLikes.PostId)
        //counter ON counter.PostId = Posts.Id
        //ORDER BY counter.Count DESC;

        public async Task<IEnumerable<PostDTO>> GetAll(string sortBy = "likes", string order = "desc",
            string searchQuery = "", int pageSize = 20, int page = 1)
        {
            var postList = (await _dbContext.Posts
                .Where(x => EF.Functions.Like(x.Body, $"%{searchQuery}%"))
                .Skip(pageSize * (page < 1 ? 0 : page - 1))
                .Take(pageSize)
                .Include(x => x.User)
                .Include(x => x.Comments)
                    .ThenInclude(c => c.User)
                .Include(x => x.Likes)
                .Include(x => x.Dislikes)
                .ToListAsync())
                .Select(x => x.ToDTO());

            return (order.ToLower() == "asc") ?
                postList.OrderBy(SortSelector(sortBy)) :
                postList.OrderByDescending(SortSelector(sortBy));
        }

        private Func<PostDTO, IComparable> SortSelector(string sortby) =>
        sortby.ToLower() switch
        {
            "likes" => x => x.Likes,
            "dislikes" => x => x.Dislikes,
            "title" => x => x.Title,
            "date" => x => x.CreatedOn,

            _ => x => x.Id
        };

        public async Task<PostDTO> CreatePost(PostDTO postDTO)
        {
            if (postDTO == null)
                return null;

            Post toAdd = postDTO.ToModel();
            toAdd.CreatedOn = DateTime.Now;

            await _dbContext.Posts.AddAsync(toAdd);
            await _dbContext.SaveChangesAsync();

            return postDTO;
        }

        public async Task<bool> DeletePost(int postId)
        {
            Post postToDelete = await _dbContext.Posts
                .FirstOrDefaultAsync(p => p.Id == postId);

            if (postToDelete == null)
            {
                return false;
            }

            this.RemoveAllRatings(postId);
            this.RemoveAllComments(postId);

            _dbContext.Posts.Remove(postToDelete);
            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<PostDTO> EditPost(int postId, PostDTO postDTO)
        {
            Post postToEdit = await _dbContext.Posts
                .FirstOrDefaultAsync(p => p.Id == postId);

            if (postToEdit == null || postDTO == null)
                return null;

            postToEdit.Id = postDTO.Id;
            postToEdit.Title = postDTO.Title;
            postToEdit.Body = postDTO.Body;
            postToEdit.EditedOn = DateTime.Now;

            await _dbContext.SaveChangesAsync();

            return postDTO;
        }

        public async Task<PostDTO> GetFirstPost(int userId)
        {
            Post post = await _dbContext.Posts
                .Include(x => x.User)
                .Include(x => x.Comments)
                    .ThenInclude(c => c.User)
                .Include(x => x.Likes)
                .Include(x => x.Dislikes)
                .FirstOrDefaultAsync(p => p.UserId == userId);

            if (post == null)
                return null;

            return post.ToDTO();
        }

        public async Task<ICollection<PostDTO>> GetUserPosts(int userId)
        {
            var postsList = await _dbContext.Posts
                .Where(x => x.UserId == userId)
                .Include(x => x.User)
                .Include(x => x.Comments)
                    .ThenInclude(c => c.User)
                .Include(x => x.Likes)
                .Include(x => x.Dislikes)
                .Select(po => po.ToDTO())
                .ToListAsync();

            return postsList;
        }

        public async Task<IEnumerable<PostDTO>> GenerateFeed(int userId, int pageSize = 20, int page = 1)
        {
            ICollection<UserDTO> friendsList = await _friendsService.GetFriends(userId);
            List<PostDTO> userFeed = new List<PostDTO>();

            foreach (UserDTO friend in friendsList)
            {
                userFeed.AddRange(await this.GetUserPosts(friend.Id));
            }

            userFeed.AddRange(await this.GetUserPosts(userId));

            return userFeed
                .Skip(pageSize * (page < 1 ? 0 : page - 1))
                .Take(pageSize);
        }

        public async Task<PostDTO> GetPost(int postId)
        {
            Post post = await _dbContext.Posts
                .Include(x => x.User)
                .Include(x => x.Comments)
                    .ThenInclude(c => c.User)
                .Include(x => x.Likes)
                .Include(x => x.Dislikes)
                .FirstOrDefaultAsync(p => p.Id == postId);

            if (post == null)
                return null;

            return post.ToDTO();
        }

        private void RemoveAllRatings(int postId)
        {
            foreach (PostLike like in _dbContext.PostLikes.Where(x => x.PostId == postId))
            {
                _dbContext.Remove(like);
            }

            foreach (PostDislike dislike in _dbContext.PostDislikes.Where(x => x.PostId == postId))
            {
                _dbContext.Remove(dislike);
            }
        }

        private void RemoveAllComments(int postId)
        {
            foreach (Comment comment in _dbContext.Comments.Where(x => x.PostId == postId))
            {
                _dbContext.Remove(comment);
            }
        }
    }
}
