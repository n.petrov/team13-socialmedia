﻿using Microsoft.EntityFrameworkCore;
using SocialMedia.Database;
using SocialMedia.Models;
using SocialMedia.Services.Contracts;
using SocialMedia.Services.DTOs;
using SocialMedia.Services.Mapper;
using System.Threading.Tasks;

namespace SocialMedia.Services
{
    public class FavoritesService : IFavoritesService
    {
        private readonly DBContext _dbContext;

        public FavoritesService(DBContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<FavoritesListDTO> AddToFavoritesList(int userId, int postId)
        {
            var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == userId);
            if (user == null)
                return null;

            var post = await _dbContext.Posts.FirstOrDefaultAsync(x => x.Id == postId);
            if (post == null)
            {
                return null;               
            }
            FavoritesList postToAdd = new FavoritesList
            {
                UserId = userId,
                PostId = postId
            };

            //if (postToAdd == null)
            //    return null;

            await _dbContext.FavoritesList.AddAsync(postToAdd);
            await _dbContext.SaveChangesAsync();

            return postToAdd.ToDTO();
        }

        public async Task<bool> DeleteFromFavoritesList(int userId, int postId)
        {
            FavoritesList postToRemove = await _dbContext.FavoritesList
                .FirstOrDefaultAsync(x => x.UserId == userId && x.PostId == postId);

            if (postToRemove == null)
            {
                return false;
            }

            _dbContext.FavoritesList.Remove(postToRemove);
            await _dbContext.SaveChangesAsync();

            return true;
        }
    }
}
