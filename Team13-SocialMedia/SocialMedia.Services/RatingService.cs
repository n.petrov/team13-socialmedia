﻿using Microsoft.EntityFrameworkCore;
using SocialMedia.Database;
using SocialMedia.Models;
using SocialMedia.Services.Contracts;
using System.Threading.Tasks;

namespace SocialMedia.Services
{
    public class RatingService : IRatingService
    {
        private readonly DBContext _dbContext;

        public RatingService(DBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> LikePost(int userId, int postId)
        {
            if (!await AreValidPost(userId, postId))
                return false;

            byte status = await ValidateStatePost(userId, postId);

            if (status == 0)
            {
                await RemovePostLike(userId, postId);
                return true;
            }
            else if (status == 1)
            {
                await RemovePostDislike(userId, postId);
            }

            PostLike toAdd = new PostLike
            {
                UserId = userId,
                PostId = postId
            };

            await _dbContext.PostLikes.AddAsync(toAdd);
            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DislikePost(int userId, int postId)
        {
            if (!await AreValidPost(userId, postId))
                return false;

            byte status = await ValidateStatePost(userId, postId);

            if (status == 1)
            {
                await RemovePostDislike(userId, postId);
                return true;
            }
            else if (status == 0)
            {
                await RemovePostLike(userId, postId);
            }

            PostDislike toAdd = new PostDislike
            {
                UserId = userId,
                PostId = postId
            };

            await _dbContext.PostDislikes.AddAsync(toAdd);
            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> LikeComment(int userId, int commentId)
        {
            if (!await AreValidComment(userId, commentId))
                return false;

            byte status = await ValidateStateComment(userId, commentId);

            if (status == 0)
            {
                await RemoveCommentLike(userId, commentId);
                return true;
            }
            else if (status == 1)
            {
                await RemoveCommentLike(userId, commentId);
            }
            else if (status == 3)
            {
                return false;
            }

            CommentLike toAdd = new CommentLike
            {
                UserId = userId,
                CommentId = commentId
            };

            await _dbContext.CommentLikes.AddAsync(toAdd);
            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DislikeComment(int userId, int commentId)
        {
            if (!await AreValidComment(userId, commentId))
                return false;

            byte status = await ValidateStateComment(userId, commentId);

            if (status == 1)
            {
                await RemoveCommentDislike(userId, commentId);
                return true;
            }
            else if (status == 0)
            {
                await RemoveCommentLike(userId, commentId);
            }
            else if (status == 3)
            {
                return false;
            }

            CommentDislike toAdd = new CommentDislike
            {
                UserId = userId,
                CommentId = commentId
            };

            await _dbContext.CommentDislikes.AddAsync(toAdd);
            await _dbContext.SaveChangesAsync();

            return true;
        }

        #region RemovalSection

        /// <summary>
        /// Removes Post Dislike from table.
        /// </summary>
        /// <returns>false if no such dislike exists.</returns>
        private async Task<bool> RemovePostDislike(int userId, int postId)
        {
            PostDislike toRemove = await _dbContext.PostDislikes
                .FirstOrDefaultAsync(x => x.UserId == userId && x.PostId == postId);

            if (toRemove == null)
                return false;

            _dbContext.PostDislikes.Remove(toRemove);
            await _dbContext.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// Removes Comment Dislike from table.
        /// </summary>
        /// <returns>false if no such dislike exists.</returns>
        private async Task<bool> RemoveCommentDislike(int userId, int commentId)
        {
            CommentDislike toRemove = await _dbContext.CommentDislikes
                .FirstOrDefaultAsync(x => x.UserId == userId && x.CommentId == commentId);

            if (toRemove == null)
                return false;

            _dbContext.CommentDislikes.Remove(toRemove);
            await _dbContext.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// Removes Post Like from table.
        /// </summary>
        /// <returns>false if no such like exists.</returns>
        private async Task<bool> RemovePostLike(int userId, int postId)
        {
            PostLike toRemove = await _dbContext.PostLikes
                .FirstOrDefaultAsync(x => x.UserId == userId && x.PostId == postId);

            if (toRemove == null)
                return false;

            _dbContext.PostLikes.Remove(toRemove);
            await _dbContext.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// Removes Comment Like from table.
        /// </summary>
        /// <returns>false if no such like exists.</returns>
        private async Task<bool> RemoveCommentLike(int userId, int commentId)
        {
            CommentLike toRemove = await _dbContext.CommentLikes
                .FirstOrDefaultAsync(x => x.UserId == userId && x.CommentId == commentId);

            if (toRemove == null)
                return false;

            _dbContext.CommentLikes.Remove(toRemove);
            await _dbContext.SaveChangesAsync();

            return true;
        }

        #endregion

        #region ValidationSection

        /// <summary>
        /// Checks whether or not given userId and postId exist.
        /// </summary>
        /// <returns>false if either does not exist.</returns>
        private async Task<bool> AreValidPost(int userId, int postId)
        {
            bool isValid = true;

            isValid &= await _dbContext.Users.AnyAsync(x => x.Id == userId);
            isValid &= await _dbContext.Posts.AnyAsync(x => x.Id == postId);

            return isValid;
        }

        /// <summary>
        /// Checks whether or not given userId and commentId exist.
        /// </summary>
        /// <returns>false if either does not exist.</returns>
        private async Task<bool> AreValidComment(int userId, int commentId)
        {
            bool isValid = true;

            isValid &= await _dbContext.Users.AnyAsync(x => x.Id == userId);
            isValid &= await _dbContext.Comments.AnyAsync(x => x.Id == commentId);

            return isValid;
        }

        /// <summary>
        /// Checks whether a user has liked or disliked a post.
        /// </summary>
        /// <returns>byte based on status:
        /// <br>0 = User has liked post.</br>
        /// <br>1 = User has disliked post.</br>
        /// <br>2 = No relation exists.</br>
        /// </returns>
        private async Task<byte> ValidateStatePost(int userId, int postId)
        {
            if (await _dbContext.PostLikes.AnyAsync(x => x.UserId == userId && x.PostId == postId))
                return 0;

            if (await _dbContext.PostDislikes.AnyAsync(x => x.UserId == userId && x.PostId == postId))
                return 1;

            return 2;
        }

        /// <summary>
        /// Checks whether a user has liked or disliked a comment.
        /// </summary>
        /// <returns>byte based on status:
        /// <br>0 = User has liked comment.</br>
        /// <br>1 = User has disliked comment.</br>
        /// <br>2 = No relation exists.</br>
        /// <br>3 = Comment has been deleted.</br>
        /// </returns>
        private async Task<byte> ValidateStateComment(int userId, int commentId)
        {
            if (await _dbContext.CommentLikes.AnyAsync(x => x.UserId == userId && x.CommentId == commentId))
                return 0;

            if (await _dbContext.CommentDislikes.AnyAsync(x => x.UserId == userId && x.CommentId == commentId))
                return 1;

            if ((await _dbContext.Comments.FirstOrDefaultAsync(x => x.Id == commentId)).UserId == null)
                return 3;

            return 2;
        }

        #endregion
    }
}
