﻿using SocialMedia.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SocialMedia.Services.Contracts
{
    public interface ICommentService
    {
        /// <summary>
        /// Adds comment to post with given Id. Commenter (user) Id must be specified.
        /// </summary>
        /// <param name="postId"></param>
        /// <param name="body">The comment's text.</param>
        /// <returns>The comment's text.</returns>
        public Task<string> ReplyToPost(int postId, int userId, string body);

        /// <summary>
        /// Edits comment with given Id.
        /// </summary>
        /// <param name="commentId">Comment Id</param>
        /// <param name="body">Comment text</param>
        /// <returns>false if comment is not found.</returns>
        public Task<bool> Edit(int commentId, string body);

        /// <summary>
        /// Does not remove comment from database but sets the userId to null and body to [deleted] to keep a comments chain's integrity.
        /// </summary>
        /// <param name="commentId">Comment Id</param>
        /// <returns>false if comment is not found.</returns>
        public Task<bool> Remove(int commentId);

        /// <summary>
        /// Returns a collection of all comments attached to post with given Id.
        /// </summary>
        /// <param name="postId"></param>
        /// <returns></returns>
        public Task<ICollection<CommentDTO>> GetPostComments(int postId);
    }
}
