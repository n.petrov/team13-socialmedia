﻿using System.Threading.Tasks;

namespace SocialMedia.Services.Contracts
{
    public interface IRatingService
    {
        /// <summary>
        /// Adds like to Post Likes table. If user has disliked change to like. If user has liked, remove like.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="postId"></param>
        /// <returns>false if either user or post does not exist.</returns>
        public Task<bool> LikePost(int userId, int postId);

        /// <summary>
        /// Adds dislike to Post Likes table. If user has liked changed to dislike. If user has disliked, remove dislike.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="postId"></param>
        /// <returns>false if either user or post does not exist.</returns>
        public Task<bool> DislikePost(int userId, int postId);

        /// <summary>
        /// Adds dislike to Comment Likes table. If user has liked changed to dislike. If user has disliked, remove dislike.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="postId"></param>
        /// <returns>false if either user or post does not exist.</returns>
        public Task<bool> LikeComment(int userId, int postId);

        /// <summary>
        /// Adds dislike to Comment Dislikes table. If user has liked changed to dislike. If user has disliked, remove dislike.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="postId"></param>
        /// <returns>false if either user or post does not exist.</returns>
        public Task<bool> DislikeComment(int userId, int postId);
    }
}
