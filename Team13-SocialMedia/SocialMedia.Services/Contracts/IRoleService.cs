﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace SocialMedia.Services.Contracts
{
    public interface IRoleService
    {
        /// <summary>
        /// Gets the Role of a user with given Id.
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns>The role of the user in the form of a string. If no user is found the string will have its default value.</returns>
        public Task<string> GetRole(int userId);

        /// <summary>
        /// Returns a list of all user roles.
        /// </summary>
        /// <returns>List of strings.</returns>
        public Task<List<string>> GetAllRoles();
    }
}
