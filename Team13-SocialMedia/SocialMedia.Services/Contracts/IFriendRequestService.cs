﻿using SocialMedia.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SocialMedia.Services.Contracts
{
    public interface IFriendRequestService
    {
        /// <summary>
        /// Creates a new entry in the db.
        /// </summary>
        /// <param name="senderId">Id of the sending user.</param>
        /// <param name="receiverId">Id of the receiving user.</param>
        /// <returns>byte based on status:
        /// <br>0 = Request already sent.</br>
        /// <br>1 = Sender has pending request from receiver.</br>
        /// <br>2 = Request successfully sent.</br>
        /// <br>3 = Users already friends.</br>
        /// </returns>
        public Task<byte> SendRequest(int senderId, int receiverId, string requestBody = "I would like to be your friend!");

        /// <summary>
        /// Removes pending request from db and adds a friend relationship as defined in the FriendRequest.
        /// </summary>
        /// <param name="requestId">Id of the request.</param>
        /// <returns>false if either a friend relationship already exists or such a request was not found.</returns>
        public Task<bool> AcceptRequest(int requestId);

        /// <summary>
        /// Removes pending/sent request from db.
        /// </summary>
        /// <param name="requestId">Id of the request.</param>
        /// <returns>false if such a request was not found.</returns>
        public Task<bool> RemoveRequest(int requestId);

        /// <summary>
        /// Retreives all outgoing requests from user with given Id.
        /// </summary>
        /// <param name="userId">Id of the user.</param>
        /// <returns>Collection of FriendRequestDTO.</returns>
        public Task<ICollection<FriendRequestDTO>> GetOutgoingRequests(int userId);

        /// <summary>
        /// Retreives all incoming requests to user with given Id.
        /// </summary>
        /// <param name="userId">Id of the user.</param>
        /// <returns>Collection of FriendRequestDTO.</returns>
        public Task<ICollection<FriendRequestDTO>> GetIncomingRequests(int userId);
    }
}
