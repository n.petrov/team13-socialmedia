﻿using SocialMedia.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SocialMedia.Services.Contracts
{
    public interface IUserService
    {
        /// <summary>
        /// Returns a single user with given Id.
        /// </summary>
        /// <param name="id">User Id</param>
        /// <returns>null if user was not found.</returns>
        public Task<UserDTO> Get(int id);

        /// <summary>
        /// Returns a collection of all users sorted by given value and ordered as specified. Search by name.
        /// </summary>
        /// <param name="sortBy">Value to sort by.</param>
        /// <param name="order">Order to sort in.</param>
        /// <param name="searchQuery">Search query.</param>
        /// <returns>Collection of UserDTO</returns>
        public Task<IEnumerable<UserDTO>> GetMany(string sortBy = "Id", string order = "asc", string searchQuery = "");

        /// <summary>
        /// Adds user to the database.
        /// </summary>
        /// <param name="userDTO">DTO Corresponding to the user to add.</param>
        /// <returns>UserDTO</returns>
        public Task<UserDTO> Create(UserDTO userDTO);

        /// <summary>
        /// Updates user with given id based on data supplied by the DTO. All existing data will be replaced except the Id which cannot be changed.
        /// </summary>
        /// <param name="id">User Id</param>
        /// <param name="userDTO">DTO containing all update information.</param>
        /// <returns>true if the user is found. If no such user exists returns false.</returns>
        public Task<bool> Update(int id, UserDTO userDTO);

        /// <summary>
        /// Removes user with given Id from the database.
        /// </summary>
        /// <param name="id">User Id</param>
        /// <returns>true if the user is found. If no such user exists returns false.</returns>
        public Task<bool> RemoveFromDb(int id);

        /// <summary>
        /// Bans user with given Id and sets a ban dscription.
        /// </summary>
        /// <param name="id">User Id</param>
        /// <param name="description">Ban description.</param>
        /// <returns>true if the user is found. If no such user exists returns false.</returns>
        public Task<bool> Ban(int id, string description);

        /// <summary>
        /// Unbans user with given Id.
        /// </summary>
        /// <param name="id">User Id</param>
        /// <returns>true if the user is found. If no such user exists returns false.</returns>
        public Task<bool> Unban(int id);

        /// <summary>
        /// Changes password of user with given Id.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="password"></param>
        /// <returns>false if no such user exists.</returns>
        public Task<bool> ChangePassword(int userId, string oldPassword, string newPassword);

        /// <summary>
        /// Updates a user's privacy setting.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="privacySetting"></param>
        /// <returns>false if no such user exists.</returns>
        public Task<bool> UpdatePrivacy(int userId, byte privacySetting);

        /// <summary>
        /// Changed a user's role.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="role"></param>
        /// <returns>false if no such user exists.</returns>
        public Task<bool> ChangeRole(int userId, string role);
    }
}