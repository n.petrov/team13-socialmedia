﻿using SocialMedia.Services.DTOs;
using System.Threading.Tasks;

namespace SocialMedia.Services.Contracts
{
    public interface IFavoritesService
    {
        /// <summary>
        /// Adds post with given Id to user with given Id's list of favorites.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="postId"></param>
        /// <returns>null if either user or post do not exist or if relation already exists.</returns>
        public Task<FavoritesListDTO> AddToFavoritesList(int userId, int postId);

        /// <summary>
        /// Deletes post with given Id from user with given Id's list.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="postId"></param>
        /// <returns>null if relation does not exist.</returns>
        public Task<bool> DeleteFromFavoritesList(int userId, int postId);
    }
}
