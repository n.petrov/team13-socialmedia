﻿using SocialMedia.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SocialMedia.Services.Contracts
{
    public interface ICountryService
    {
        /// <summary>
        /// Retreives country with given Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>null if country with given Id does not exist.</returns>
        public Task<CountryDTO> Get(int id);

        /// <summary>
        /// Retreives all countries
        /// </summary>
        /// <returns>ICollection of CountryDTOs.</returns>
        public Task<ICollection<CountryDTO>> GetMany();

        /// <summary>
        /// Add country to database.
        /// </summary>
        /// <param name="countryDTO"></param>
        /// <returns>null if country already exists.</returns>
        public Task<CountryDTO> Create(CountryDTO countryDTO);

        /// <summary>
        /// Updates country with given Id with information supplied by DTO.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="countryDTO"></param>
        /// <returns>null if country was not found.</returns>
        public Task<CountryDTO> Update(int id, CountryDTO countryDTO);

        /// <summary>
        /// Removes country with given Id from database.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>false if no such country exists.</returns>
        public Task<bool> Delete(int id);
    }
}
