﻿using SocialMedia.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SocialMedia.Services.Contracts
{
    public interface IFriendsService
    {
        /// <summary>
        /// Creates friend relation between two users in the database. Parameter order does not matter.
        /// </summary>
        /// <returns>false if no user with either Id exists. false if relation already exists.</returns>
        public Task<bool> AddFriend(int user1Id, int user2Id);

        /// <summary>
        /// Checks whether a friend relation exists between two users. Parameter order does not matter.
        /// </summary>
        /// <returns>true if relation exists.</returns>
        public Task<bool> IsFriendsWith(int user1Id, int user2Id);

        /// <summary>
        /// Removes friend relationship between User with userId and user with FriendId from database. Parameter order does not matter.
        /// </summary>
        /// <returns>false if no such relation exists.</returns>
        public Task<bool> DeleteFriend(int user1Id, int user2Id);

        /// <summary>
        /// Retreives all friends a user with given Id has.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>Collection of User DTOs</returns>
        public Task<ICollection<UserDTO>> GetFriends(int userId);
    }
}
