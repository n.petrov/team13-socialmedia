﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace SocialMedia.Services.Contracts
{
    public interface IPictureService
    {
        /// <summary>
        /// Edits user profile picture.
        /// </summary>
        /// <returns>false if inputs are invalid.</returns>
        public Task<bool> EditProfilePicture(int userId, IFormFile file);

        /// <summary>
        /// Edits post picture picture.
        /// </summary>
        /// <returns>false if inputs are invalid.</returns>
        public Task<bool> EditPostPicture(int postId, IFormFile file);

        /// <summary>
        /// Returns a tuple with given file data as byte[] and the data type as string.
        /// </summary>
        /// <param name="file">File to convert.</param>
        /// <returns>(byte[], string). Default values are byte[1], "image/png".</returns>
        public Task<(byte[] Data, string DataType)> GetFileData(IFormFile file);
    }
}
