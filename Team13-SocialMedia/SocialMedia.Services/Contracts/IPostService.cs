﻿using SocialMedia.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SocialMedia.Services.Contracts
{
    public interface IPostService
    {
        /// <summary>
        /// Returns the first post made by user with given id.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>null if no such post exists.</returns>
        public Task<PostDTO> GetFirstPost(int userId);

        /// <summary>
        /// Returns post with given Id.
        /// /// </summary>
        /// <param name="postId"></param>
        /// <returns>null if post was not found.</returns>
        public Task<PostDTO> GetPost(int postId);

        /// <summary>
        /// Returns a list of all posts made by user with given Id.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>ICollection of PostDTOs.</returns>
        public Task<ICollection<PostDTO>> GetUserPosts(int userId);

        /// <summary>
        /// Populates a collection of posts based on a user's friends and follows. Implements pagination.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="pageSize">Amount of elements to display per page.</param>
        /// <param name="page">Which page to display.</param>
        /// <returns>ICollection of PostDTOs.</returns>
        public Task<IEnumerable<PostDTO>> GenerateFeed(int userId, int pageSize = 20, int page = 1);

        /// <summary>
        /// Adds post to database.
        /// </summary>
        /// <param name="postDTO"></param>
        /// <returns>null if argument was null</returns>
        public Task<PostDTO> CreatePost(PostDTO postDTO);

        /// <summary>
        /// Edits post with given Id based on the supplied DTO's properties.
        /// </summary>
        /// <param name="postId">Id of the post to edit.</param>
        /// <param name="postDTO">DTO based on which to edit values.</param>
        /// <returns>null if either the post was not found or the DTO is null.</returns>
        public Task<PostDTO> EditPost(int postId, PostDTO postDTO);

        /// <summary>
        /// Removes post with given Id from database.
        /// </summary>
        /// <param name="postId"></param>
        /// <returns>false if post was not found.</returns>
        public Task<bool> DeletePost(int postId);

        /// <summary>
        /// Retrieves all posts in the database.
        /// </summary>
        /// <returns>ICollection of PostDTO.</returns>
        public Task<ICollection<PostDTO>> GetAll();

        /// <summary>
        /// Retrieves all posts in the database. Implements pagination and sorting.
        /// </summary>
        /// <param name="sortBy">Sort parameter. Could be by 'likes', 'dislikes', 'title' and 'date'.</param>
        /// <param name="order">Order in which to sort. Could be 'asc' or 'desc'.</param>
        /// <param name="searchQuery">Search query.</param>
        /// <param name="pageSize">Amount of elements to display per page.</param>
        /// <param name="page">Which page to display.</param>
        /// <returns>IEnumerable of PostDTO.</returns>
        public Task<IEnumerable<PostDTO>> GetAll(string sortBy = "likes", string order = "desc",
            string searchQuery = "", int pageSize = 20, int page = 1);
    }
}
