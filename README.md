### Trello
https://trello.com/b/gWvZS1BA

### Database Diagram

![](./img/diagram.png)


# Dolphin+

Dolphin+ is a social media platform that provides all the functionality you would expect. You can do everything from finding and adding friends to creating posts and sharing information about yourself. 

# Features

  - Find/Add friend
  - Create text/media posts
  - Rate existing posts and comments
  - Comment under posts

You can also:
  - Edit your profile to share information about yourself
  - Search for specific posts

# API

You some site data via API calls:

Comments:

```sh
PUT    /api/Comments/{commentid}
POST   /api/Comments/{commentid}
DELETE /api/Comments/{id}
```

Friends:

```sh
/api/Friends
```

Posts:

```sh
GET  /api/Posts
POST /api​/Posts
GET  /api/Posts/newsfeed
```

Users:

```sh
GET /api/Users/{id}
GET /api/Users
```

You can also test any of the calls in the provided API environment.

![](./img/swagger.png)

# Website


The register and login forms allow you to register and login.


![](./img/login.png)
![](./img/register.png)


The navbar shows you the site's navigation. It will display additional elements based on wether or not a use is signed in and that user's role.


![](./img/navbar.png)


You can browse/edit your profile and logout by expanding the user menu.


![](./img/usrmenu.png)


Any friend requests be it incoming or outgoing can be seen in the navbar.


![](./img/frreq.png)


All requests and friends can be managed in the friends page.


![](./img/frpage.png)


You can look for friends by using the provided search in the navbar.


![](./img/frsearch.png)


Any posts can be found be using the post search in the navbar.


![](./img/postsearch.png)